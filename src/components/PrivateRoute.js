import React from "react";
import {Redirect, Route} from "react-router-dom";

export default function PrivateRoute({component: Component, isAuth, user, ...rest}) {
    return (
        <Route
            {...rest}
            render={props =>
                isAuth ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/auth",
                            state: {from: props.location}
                        }}
                    />
                )
            }
        />
    );
}
