import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {collapseSidebar} from "../actions/page";
import connect from "react-redux/es/connect/connect";
import {logout} from "../actions/user";
import {can} from "../helpers/user";

class Header extends Component {

    collapseSidebarHandler = (e) => {
        e.preventDefault();

        const {dispatch} = this.props;

        dispatch(collapseSidebar());
    };

    logout = () => {
        const {dispatch} = this.props;
        dispatch(logout());
    };

    render() {
        return (
            <>
                <nav className="main-header navbar navbar-expand bg-white navbar-light border-bottom">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link" onClick={this.collapseSidebarHandler}>
                                <i className="fa fa-bars"/>
                            </a>
                        </li>
                        <li className="nav-item d-none d-sm-inline-block">
                            <Link to="/" className="nav-link">Home</Link>
                        </li>

                    </ul>
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <a onClick={this.logout} className="nav-link">
                                <i className="fa fa-sign-out"/>
                            </a>
                        </li>
                    </ul>
                </nav>
                <aside className="main-sidebar sidebar-dark-primary elevation-4">
                    <Link to="/" className="brand-link">
                        <img src="/images/logo_s.png" alt="Test Maker" className="brand-image"/>
                        <span className="brand-text font-weight-light">Test Maker</span>
                    </Link>

                    <div className="sidebar">
                        {this.props.user.user && <div className="user-panel mt-3 pb-3 mb-3 d-flex">
                            <div className="image">
                                <img src={this.props.user.user.imageUrl} className="img-circle elevation-2"
                                     alt="User Image"/>
                            </div>
                            <div className="info">
                                <Link to="/profile" className="d-block">
                                    {this.props.user.user.name}
                                    {this.props.user.user.isAdmin && <span className="badge bg-danger">Admin</span>}
                                </Link>
                            </div>
                        </div>}
                        <nav className="mt-2">
                            <ul className="nav nav-pills nav-sidebar flex-column" role="menu">
                                <li className="nav-item">
                                    <Link to="/tests" className="nav-link">
                                        <i className="nav-icon fa fa-list text-info"/>
                                        <p>Tests</p>
                                    </Link>
                                </li>
                                {can(this.props.user.user) && <li className="nav-item">
                                    <Link to="/users" className="nav-link">
                                        <i className="nav-icon fa fa-users text-info"/>
                                        <p>Users</p>
                                    </Link>
                                </li>}
                            </ul>
                        </nav>
                    </div>
                </aside>
            </>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    page: state.page,
    user: state.user,
});

export default connect(mapStateToProps)(Header);
