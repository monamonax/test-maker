import React, {Component} from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="main-footer">
                <div className="float-right">Сourse work. Goryavin M.</div>
                <strong>Copyright &copy; 2018 </strong> All rights
                reserved.
            </footer>
        );
    }
}

export default Footer;
