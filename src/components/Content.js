import React, {Component} from 'react';

class Content extends Component {
    render() {
        const {children} = this.props;
        return (
            <div className="content-wrapper" style={{minHeight: 843 + 'px'}}>
                {children}
            </div>
        );
    }
}

export default Content;
