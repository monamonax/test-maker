import React, {Component} from "react";
import {Alert, Intent} from '@blueprintjs/core';
import * as TestConstants from "../../constants/test";
import 'moment-timezone';
import connect from "react-redux/es/connect/connect";
import {changeStatusTest, deleteTest} from "../../actions";
import {Link, Redirect, withRouter} from 'react-router-dom';
import {createStatusTabByStatusId, createTimeBlock, getResultByUserId, isAvailabilityAction} from "../../helpers/test";

class TestListItem extends Component {
    static defaultProps = {
        test: null,
        user: {
            user: null,
        },
        deleteModalIsOpen: false,
        runTestConfirmModalIsOpen: false,
    };

    constructor(props) {
        super(props);
        this.state = {
            cardIsOpen: props.cardIsOpen,
            deleteModalIsOpen: props.deleteModalIsOpen,
            runTestConfirmModalIsOpen: props.runTestConfirmModalIsOpen,
            redirect: null,
            result: getResultByUserId(this.props.test, this.props.user.user.id)
        }
    };

    delete = () => {
        const {dispatch} = this.props;
        dispatch(deleteTest(this.props.test, () => {
            this.toggleDeleteModal();
        }));
    };

    runTest = () => {
        this.toggleRunTestConfirmModal();
        this.setState({redirect: `/tests/${this.props.test.id}/testing`});
    };

    changeStatus = (statusId) => {
        const {dispatch} = this.props;
        dispatch(changeStatusTest(this.props.test.id, statusId, (test) => {

        }));
    };

    toggleDeleteModal = () => this.setState({deleteModalIsOpen: !this.state.deleteModalIsOpen});
    toggleRunTestConfirmModal = () => this.setState({runTestConfirmModalIsOpen: !this.state.runTestConfirmModalIsOpen});

    onClickDeleteHandler = (e) => {
        e.preventDefault();
        this.toggleDeleteModal();
    };
    onClickDeleteConfirmHandler = (e) => {
        e.preventDefault();
        this.delete();
    };
    onClickChangeStatusHandler = (statusId, action) => (e) => {
        e.preventDefault();
        if (action) {
            if (isAvailabilityAction(action, this.props.test)) {
                this.changeStatus(statusId);
            }
        } else {
            this.changeStatus(statusId);
        }
    };

    onClickRunTestHandler = (e) => {
        e.preventDefault();
        this.toggleRunTestConfirmModal();
    };
    onClickRunTestConfirmHandler = (e) => {
        e.preventDefault();
        this.runTest();
    };

    renderAdminActions = () => {
        return (
            <>
                <div className="btn-group btn-group-sm mr-2" role="group" aria-label="">
                    {
                        isAvailabilityAction('edit', this.props.test) ?
                            <Link to={`/tests/${this.props.test.id}/update`} className="btn btn-info btn-flat"
                                  disabled={!isAvailabilityAction('edit', this.props.test)}><i className="fa fa-edit"/></Link>
                            :
                            <button type="button" className="btn btn-info btn-flat"
                                    disabled={!isAvailabilityAction('edit', this.props.test)} title="Edit"><i
                                className="fa fa-edit"/></button>
                    }
                    {
                        isAvailabilityAction('view', this.props.test) ?
                            <Link to={`/tests/${this.props.test.id}/view`} className="btn btn-primary btn-flat"
                                  disabled={!isAvailabilityAction('view', this.props.test)}>View</Link>
                            :
                            <button type="button" className="btn btn-primary btn-flat"
                                    disabled={!isAvailabilityAction('view', this.props.test)}>View</button>
                    }
                    {
                        isAvailabilityAction('ready', this.props.test) &&
                        <button type="button" className="btn btn-success btn-flat"
                                onClick={this.onClickChangeStatusHandler(TestConstants.STATUS_READY_RUN, 'ready')}>Ready
                            to run</button>
                    }
                    {
                        isAvailabilityAction('run', this.props.test) &&
                        <button type="button" className="btn btn-success btn-flat"
                                onClick={this.onClickChangeStatusHandler(TestConstants.STATUS_RUNNING, 'run')}>Run</button>
                    }
                    {
                        isAvailabilityAction('end', this.props.test) &&
                        <button type="button" className="btn btn-success btn-flat"
                                onClick={this.onClickChangeStatusHandler(TestConstants.STATUS_END, 'end')}>End</button>
                    }
                    <div className="btn-group btn-group-sm" role="group" aria-label="">
                        <Link to={`/tests/${this.props.test.id}/results`}
                              className="btn btn-success btn-flat">Results</Link>
                    </div>
                    <button type="button" className="btn btn-danger btn-flat"
                            disabled={!isAvailabilityAction('delete', this.props.test)}
                            onClick={this.onClickDeleteHandler}>Delete
                    </button>
                </div>
                <Alert
                    cancelButtonText="Cancel"
                    confirmButtonText="Delete"
                    icon="trash"
                    intent={Intent.DANGER}
                    isOpen={this.state.deleteModalIsOpen}
                    onCancel={this.onClickDeleteHandler}
                    onConfirm={this.onClickDeleteConfirmHandler}
                >
                    <p>
                        Are you sure you want to delete this test? You will be able to restore it later,
                        but it will become private to you.
                    </p>
                </Alert>
            </>
        );
    };
    renderUserActions = () => {
        if (this.props.test.status === TestConstants.STATUS_RUNNING && (this.state.result == null || (this.state.result && !this.state.result.is_end))) {
            return (
                <>
                    <div className="btn-group btn-group-sm" role="group" aria-label="">
                        <button type="button" className="btn btn-success btn-flat"
                                onClick={this.onClickRunTestHandler}>Pass the test
                        </button>
                    </div>
                    <Alert
                        cancelButtonText="Cancel"
                        confirmButtonText="Yep"
                        icon="confirm"
                        intent={Intent.WARNING}
                        isOpen={this.state.runTestConfirmModalIsOpen}
                        onCancel={this.onClickRunTestHandler}
                        onConfirm={this.onClickRunTestConfirmHandler}
                    >
                        <p>
                            Are you sure you want to start the test?
                        </p>
                    </Alert>
                </>
            );
        } else if (this.state.result != null && this.state.result.is_end) {
            return (
                <>
                    <div className="btn-group btn-group-sm" role="group" aria-label="">
                        <Link to={`/tests/${this.props.test.id}/results`} className="btn btn-success btn-flat">My
                            results</Link>
                    </div>
                </>
            );
        }

        return (
            <></>
        );
    };

    render() {
        if (this.state.redirect) {
            return <Redirect to={this.state.redirect}/>;
        }

        return (
            <tr>
                <td>{this.props.test.id}</td>
                <td>{createStatusTabByStatusId(this.props.test.status)}</td>
                <td>{this.props.test.title}</td>
                <td>{this.props.test.description}</td>
                <td>{createTimeBlock(this.props.test)}</td>
                <td style={{width: 277 + 'px'}}>
                    <div className="btn-toolbar justify-content-between" role="toolbar">
                        {this.props.user.user.isAdmin ? this.renderAdminActions() : this.renderUserActions()}
                    </div>
                </td>
            </tr>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
    };
};

export default withRouter(connect(mapStateToProps)(TestListItem));
