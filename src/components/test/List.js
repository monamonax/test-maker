import React, {Component} from "react";
import TestListItem from "./ListItem";

class TestList extends Component {
    static defaultProps = {
        items: [],
    };

    render() {
        return (
            <div className="table-responsive">
                <table className="table table-striped  table-sm">
                    <thead>
                    <tr>
                        <th style={{width: 10 + 'px'}}>#</th>
                        <th>Status</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Last update status</th>
                        <th style={{width: 277 + 'px'}}>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.items.map(test => (
                        <TestListItem test={test} key={test.id}/>
                    ))}
                    </tbody>

                </table>
            </div>
        );
    }
}

export default TestList;
