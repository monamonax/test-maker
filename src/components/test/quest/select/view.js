import React from "react";
import 'moment-timezone';
import connect from "react-redux/es/connect/connect";
import {createQuestModel} from "../../../../utils";
import Query from "../Quest.";
import renderHTML from "react-render-html";
import {createBallBadge} from "../../../../helpers/test";

var _ = require('lodash');

class SelectQuestView extends Query {
    static defaultProps = {
        result: null,
        index: null,
        testId: null,
        maxPrice: 0,
        quest: {
            ...createQuestModel({}),
            testId: null,
            data: {
                quest: "",
                items: [],
            }
        }
    };

    getClass = (item) => {
        if (item.valid) {

            return 'callout callout-success';
        }
        if (this.isChecked(item)) {
            return 'callout callout-danger';
        }

        return 'callout callout-default';
    };

    isChecked = (item) => {
        let checked = false;
        this.props.result.data.map((res) => {
            if (this.props.quest.id === res.questId) {
                const index = _.findIndex(res.items, (text) => text === item.text);
                if (index != -1) {
                    checked = true;
                    return checked;
                }
            }
        });

        return checked;
    };

    getResult = () => {
        let balls = false;
        this.props.result.result.map((res) => {
            if (this.props.quest.id === res.questId) {
                balls = res.price;
            }
        });

        return balls;
    };

    renderSumBadge = (item) => {
        const price = parseFloat(this.props.quest.price);
        const countValid = this.props.quest.data.items.reduce((count, item) => {
            return count + item.valid;
        }, 0);

        const output = (price / countValid).toFixed(2);
        if (item.valid) {
            return (<span className="badge badge-success badge-pill">{output}</span>);
        }
        return (<span className="badge badge-danger badge-pill">-{output}</span>);
    };

    render() {
        return (
            this.props.quest &&
            <div className="card card-default">
                <div className="card-header">
                    <h3 className="card-title">
                        <i className="fa fa-text"/>
                        Select one/more Quest
                    </h3>
                </div>
                <div className="card-body">
                    <div className="callout callout-info">
                        <h5>Quest</h5>
                        {renderHTML(this.props.quest.data.quest)}
                    </div>
                    <hr/>
                    {
                        this.props.quest.data.items.map((item, index) => {
                            return (
                                <div className={this.getClass(item)} key={index}>

                                    <h5 className="d-flex justify-content-between align-items-left">
                                        {this.isChecked(item) && <i className="fa fa-check"> {item.text}</i>}
                                        {!this.isChecked(item) && item.text}

                                        {this.isChecked(item) && this.renderSumBadge(item)}
                                    </h5>
                                </div>
                            );
                        })
                    }
                </div>
                <div className="card-footer">
                    <p>Balls: {createBallBadge(this.getResult(), this.props.quest.price)}</p>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({});

export default connect(mapStateToProps)(SelectQuestView);
