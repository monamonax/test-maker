import React from "react";
import {Button, Checkbox, FormGroup, InputGroup, Tag} from '@blueprintjs/core';
import 'moment-timezone';
import connect from "react-redux/es/connect/connect";
import {createQuestModel, handleBooleanChange, handleStringChange} from "../../../../utils";
import Query from "../Quest.";
import {Editor} from "@tinymce/tinymce-react/lib/es2015";

class SelectQuestEdit extends Query {
    static defaultProps = {
        editable: true,
        index: null,
        testId: null,
        removeMethod: () => {
        },
        quest: {
            ...createQuestModel({}),
            data: {
                quest: "",
                items: [],
            }
        }
    };

    addItem = () => {
        let items = this.state.quest.data.items;
        items.push({
            text: "",
            valid: false,
        });

        return this.setState(
            {
                quest: {
                    ...this.state.quest,
                    data: {
                        ...this.state.quest.data,
                        items: items,
                    }
                }
            }
        );
    };

    onChangeValidItemHandler = (index) => handleBooleanChange(value => {
        let items = this.state.quest.data.items;
        items[index].valid = value;

        return this.setState(
            {
                quest: {
                    ...this.state.quest,
                    data: {
                        ...this.state.quest.data,
                        items: items,
                    }
                }
            }
        );
    });

    onChangeItemTextHandler = (index) => handleStringChange(value => {
        let items = this.state.quest.data.items;
        items[index].text = value;

        return this.setState(
            {
                quest: {
                    ...this.state.quest,
                    data: {
                        ...this.state.quest.data,
                        success: value
                    }
                }
            }
        );
    });

    onClickAddItemHandler = (e) => {
        e.preventDefault();
        this.addItem();
    };

    renderSumBadge = (index) => {
        const price = parseFloat(this.state.quest.price);
        const countValid = this.state.quest.data.items.reduce((count, item) => {
            return count + item.valid;

        }, 0);
        const isValid = this.state.quest.data.items[index].valid;

        return (<Tag>{isValid ? (price / countValid).toFixed(2) : 0}</Tag>);
    };

    render() {
        return (
            this.state.quest &&
            <>
                <div className="form-group">
                    <FormGroup
                        label="Quest text"
                        labelFor={`selectQuest-${(this.props.index + 1)}`}
                        labelInfo="(required)">
                        <Editor
                            initialValue={this.state.quest.data.quest}
                            init={{
                                plugins: 'link image code codesample',
                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | codesample |' +
                                    ' code'
                            }}
                            onChange={this.onChangeQuestFieldHandler}
                        />
                    </FormGroup>
                </div>
                <div className="form-group">
                    <FormGroup
                        label="Price"
                        helperText="The price of the correct answer. How many points will be added for the correct answer"
                        labelFor={`selectPrice-${(this.props.index + 1)}`}
                        labelInfo="(required)">
                        <InputGroup
                            type="number"
                            disabled={!this.props.editable}
                            name={`selectPrice${(this.props.index + 1)}`}
                            id={`selectPrice-${(this.props.index + 1)}`}
                            placeholder="Enter price quest"
                            value={this.state.quest.price}
                            onChange={this.onChangePriceFieldHandler}
                            required={true}
                        />
                    </FormGroup>
                </div>
                <div className="form-group">
                    <FormGroup
                        label="Select elements."
                        labelInfo="(min 3 element, required minimum one success) (not validate)">
                        {
                            this.state.quest.data.items.map((item, index) => {
                                return <InputGroup
                                    key={index}
                                    type="text"
                                    disabled={!this.props.editable}
                                    name="title"
                                    id={`selectAnswer-${(this.props.index + 1)}-${index}`}
                                    placeholder="Enter success answer"
                                    className="bp3-fill mt-2 mb-2"
                                    rightElement={
                                        <Checkbox checked={item.valid} onChange={this.onChangeValidItemHandler(index)}
                                                  className="mr-2">Is Success {this.renderSumBadge(index)}</Checkbox>
                                    }
                                    value={item.text}
                                    onChange={this.onChangeItemTextHandler(index)}
                                    required={true}
                                />
                            })
                        }
                        <Button icon="plus" text="Add select elements" onClick={this.onClickAddItemHandler}/>
                    </FormGroup>
                </div>
                <div className="btn-group" role="group" aria-label="Actions">
                    <button disabled={!this.props.editable} className="btn btn-success btn-flat" type="button"
                            onClick={this.onClickSaveHandler}>Save only this quest
                    </button>
                </div>
            </>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({});

export default connect(mapStateToProps)(SelectQuestEdit);
