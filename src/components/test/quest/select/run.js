import React from "react";
import {Checkbox, Radio, RadioGroup} from '@blueprintjs/core';
import 'moment-timezone';
import connect from "react-redux/es/connect/connect";
import {createQuestModel, handleBooleanChange, handleStringChange} from "../../../../utils";
import renderHTML from "react-render-html";
import RunQuery from "../RunQuest";

class SelectQuestRun extends RunQuery {
    static defaultProps = {
        result: null,
        index: null,
        testId: null,
        maxPrice: 0,
        quest: {
            ...createQuestModel({}),
            testId: null,
            data: {
                quest: "",
                items: [],
            }
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            answer: this.getAnswer(),
        };
    }

    getAnswer = () => {
        let result = [];
        this.props.result.data.map((res) => {
            if (this.props.quest.id === res.questId) {
                if (res.items) {
                    result = res.items;
                }
            }
        });

        return result;
    };

    getCountValid = () => {
        let result = 0;
        this.props.quest.data.items.map((res) => {
            if (res.valid) {
                result++;
            }
        });

        return result;
    };

    isChecked = (text) => {
        return Boolean(this.state.answer.find((element) => (text === element)));
    };

    onChangeOneAnswerHandler = handleStringChange((value) => {
        this.setState({
            answer: [value],
        })
    });

    onChangeMoreAnswerHandler = (item) => handleBooleanChange((value) => {
        let answer = [];
        this.state.answer.forEach((text) => {
            if (text !== item.text) {
                answer.push(text);
            }
        });
        if (value) {
            answer.push(item.text);
        }
        this.setState({
            ...this.state,
            answer: answer,
        })
    });

    renderSelectOne() {
        return (
            <RadioGroup
                label="Select one element"
                onChange={this.onChangeOneAnswerHandler}
                selectedValue={this.state.answer[0]}
            >
                {this.props.quest.data.items.map((item, index) => {
                    return (<Radio label={item.text} value={item.text} key={index}/>);
                })}
            </RadioGroup>
        );
    };

    renderSelectMany = () => {
        return (
            <>
                {this.props.quest.data.items.map((item, index) => {
                    return (<Checkbox checked={this.isChecked(item.text)} label={item.text}
                                      onChange={this.onChangeMoreAnswerHandler(item)} key={index}/>);
                })}
            </>
        );
    };

    render() {
        return (
            this.props.quest &&
            <div className="card card-default">
                <form onSubmit={this.onSubmitAnswerHandler}>
                    <div className="card-body">
                        <div className="callout callout-info">
                            <h5>Quest</h5>
                            {renderHTML(this.props.quest.data.quest)}
                        </div>
                        <hr/>
                        <div className="callout callout-dafault">
                            {this.getCountValid() === 1 ? this.renderSelectOne() : this.renderSelectMany()}
                        </div>
                    </div>
                    <div className="card-footer">
                        <button type="submit" className="btn btn-success btn-flat">Save answer</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({});

export default connect(mapStateToProps)(SelectQuestRun);
