import 'moment-timezone';
import {answerResult} from "../../../actions";
import {Intent} from '@blueprintjs/core';
import Query from "./Quest.";
import {AppToaster} from "../../Toaster";

class RunQuery extends Query {
    constructor(props) {
        super(props);

        this.state = {
            answer: null,
        };
    };

    answer = () => {
        const {dispatch, testId, quest, result} = this.props;

        dispatch(answerResult({
            testId: testId,
            questId: quest.id,
            resultId: result.id,
            answer: this.state.answer,
        }, (result) => {
            AppToaster.show({
                message: 'Answer saved!',
                icon: "tick",
                intent: Intent.SUCCESS,
            });
        }));
    };

    onSubmitAnswerHandler = (e) => {
        e.preventDefault();
        this.answer();
    };

}

export default RunQuery;
