import React from "react";
import {FormGroup, InputGroup} from '@blueprintjs/core';
import 'moment-timezone';
import connect from "react-redux/es/connect/connect";
import {createQuestModel, handleStringChange} from "../../../../utils";
import Query from "../Quest.";

import {Editor} from '@tinymce/tinymce-react';

class StringQuestEdit extends Query {
    static defaultProps = {
        editable: true,
        index: null,
        testId: null,
        removeMethod: () => {
        },
        quest: {
            ...createQuestModel({}),
            testId: null,
            data: {
                quest: "",
                success: "",
            }
        }
    };

    onChangeAnswerFieldHandler = handleStringChange(value => this.setState(
        {
            quest: {
                ...this.state.quest,
                data: {
                    ...this.state.quest.data,
                    success: value
                }
            }
        }
    ));

    render() {
        return (
            this.state.quest &&
            <>
                <div className="form-group mt-3">
                    <FormGroup
                        label="Quest text"
                        labelFor={`stringQuest-${(this.props.index + 1)}`}
                        labelInfo="(required)">
                        <Editor
                            initialValue={this.state.quest.data.quest}
                            init={{
                                plugins: 'link image code codesample',
                                toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | codesample |' +
                                    ' code'
                            }}
                            onChange={this.onChangeQuestFieldHandler}
                        />
                    </FormGroup>
                </div>
                <div className="form-group">
                    <FormGroup
                        label="Price"
                        helperText="The price of the correct answer. How many points will be added for the correct answer"
                        labelFor={`stringPrice-${(this.props.index + 1)}`}
                        labelInfo="(required)">
                        <InputGroup
                            type="number"
                            disabled={!this.props.editable}
                            name={`stringPrice${(this.props.index + 1)}`}
                            id={`stringPrice-${(this.props.index + 1)}`}
                            placeholder="Enter price quest"
                            value={this.state.quest.price}
                            onChange={this.onChangePriceFieldHandler}
                            required={true}
                        />
                    </FormGroup>
                </div>
                <div className="form-group">
                    <FormGroup
                        label="Answer text"
                        helperText="The text of the answer when comparing will be lowercase."
                        labelFor={`stringAnswer-${(this.props.index + 1)}`}
                        labelInfo="(required)">
                        <InputGroup
                            type="text"
                            disabled={!this.props.editable}
                            name="title"
                            id={`stringAnswer-${(this.props.index + 1)}`}
                            placeholder="Enter success answer"
                            value={this.state.quest.data.success}
                            onChange={this.onChangeAnswerFieldHandler}
                            required={true}
                        />
                    </FormGroup>
                </div>
                <div className="btn-group" role="group" aria-label="Actions">
                    <button disabled={!this.props.editable} className="btn btn-success btn-flat" type="button"
                            onClick={this.onClickSaveHandler}>Save only this quest
                    </button>
                    <button disabled={!this.props.editable} className="btn btn-danger btn-flat" type="button"
                            onClick={this.onClickDeleteHandler}>Delete quest
                    </button>
                </div>
            </>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({});

export default connect(mapStateToProps)(StringQuestEdit);
