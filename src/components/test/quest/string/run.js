import React from "react";
import {FormGroup, InputGroup} from '@blueprintjs/core';
import 'moment-timezone';
import connect from "react-redux/es/connect/connect";
import {createQuestModel, handleStringChange} from "../../../../utils";
import renderHTML from "react-render-html";
import RunQuery from "../RunQuest";

class StringQuestRun extends RunQuery {
    static defaultProps = {
        result: null,
        index: null,
        testId: null,
        maxPrice: 0,
        quest: {
            ...createQuestModel({}),
            testId: null,
            data: {
                quest: "",
                success: "",
            }
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            answer: this.getAnswer(),
        };
    }

    getAnswer = () => {
        let result = '';
        this.props.result.data.map((res) => {
            if (this.props.quest.id === res.questId) {
                result = res.answer;
            }
        });

        return result;
    };

    onChangeAnswerHandler = handleStringChange((value) => {
        this.setState({
            answer: value,
        })
    });

    render() {
        return (
            this.props.quest &&
            <div className="card card-default">
                <form onSubmit={this.onSubmitAnswerHandler}>
                    <div className="card-body">
                        <div className="callout callout-info">
                            <h5>Quest</h5>
                            {renderHTML(this.props.quest.data.quest)}
                        </div>
                        <hr/>
                        <div className="callout callout-primary">
                            <h5>Your answer:</h5>

                            <div className="form-group">
                                <FormGroup
                                    helperText="The text of the answer when comparing will be lowercase."
                                    labelFor={`stringAnswer-${(this.props.index + 1)}`}
                                    labelInfo="(required)">
                                    <InputGroup
                                        type="text"
                                        name="answer"
                                        id={`stringAnswer-${(this.props.index + 1)}`}
                                        placeholder="Enter your answer"
                                        value={this.state.answer}
                                        onChange={this.onChangeAnswerHandler}
                                        required={true}
                                    />
                                </FormGroup>
                            </div>

                        </div>
                    </div>
                    <div className="card-footer">
                        <button type="submit" className="btn btn-success btn-flat">Save answer</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({});

export default connect(mapStateToProps)(StringQuestRun);
