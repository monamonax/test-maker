import React from "react";
import renderHTML from 'react-render-html';
import 'moment-timezone';
import connect from "react-redux/es/connect/connect";
import {createQuestModel} from "../../../../utils";
import Query from "../Quest.";
import {createBallBadge} from "../../../../helpers/test";

class StringQuestView extends Query {
    static defaultProps = {
        result: null,
        index: null,
        testId: null,
        maxPrice: 0,
        quest: {
            ...createQuestModel({}),
            testId: null,
            data: {
                quest: "",
                success: "",
            }
        }
    };

    getAnswer = () => {
        let result = '';
        this.props.result.data.map((res) => {
            if (this.props.quest.id === res.questId) {
                result = res.answer;
            }
        });

        return result;
    };

    isSuccess = () => {
        let success = false;
        this.props.result.result.map((res) => {
            if (this.props.quest.id === res.questId) {
                success = res.price === this.props.quest.price;
            }
        });

        return success;
    };

    getResult = () => {
        let balls = false;
        this.props.result.result.map((res) => {
            if (this.props.quest.id === res.questId) {
                balls = res.price;
            }
        });

        return balls;
    };

    getAnswerClass = () => {
        if (this.isSuccess()) {
            return 'callout callout-success';
        }
        return 'callout callout-danger';
    };

    render() {
        return (
            this.props.quest &&
            <div className="card card-default">
                <div className="card-header">
                    <h3 className="card-title">
                        <i className="fa fa-text"/>
                        String Quest
                    </h3>
                </div>
                <div className="card-body">
                    <div className="callout callout-info">
                        <h5>Quest</h5>
                        {renderHTML(this.props.quest.data.quest)}
                    </div>
                    <hr/>
                    <div className="callout callout-info">
                        <h5>Success Answer:</h5>
                        <p>{this.props.quest.data.success}</p>
                    </div>
                    <div className={this.getAnswerClass()}>
                        <h5>Your answer:</h5>

                        <p>{this.getAnswer()}</p>
                    </div>
                </div>
                <div className="card-footer">
                    <p>Balls: {createBallBadge(this.getResult(), this.props.quest.price)}</p>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({});

export default connect(mapStateToProps)(StringQuestView);
