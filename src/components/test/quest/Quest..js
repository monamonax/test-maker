import {Component} from "react";
import 'moment-timezone';
import {handleNumberChange} from "../../../utils";
import {answerResult, createQuest, deleteQuest, loadQuestsByTestId, updateQuest} from "../../../actions";
import {AppToaster} from "../../Toaster";
import {Intent} from "@blueprintjs/core";

class Query extends Component {
    constructor(props) {
        super(props);

        this.state = {
            quest: this.props.quest,
            answer: null,
        };
    };

    createQuest = () => {
        const {dispatch} = this.props;
        dispatch(createQuest(this.state.quest, (quest) => {
            this.setState({quest: {...quest, testId: this.props.testId, sort: this.props.index,}});
            dispatch(loadQuestsByTestId(this.state.quest.testId, (test) => {
                AppToaster.show({
                    message: 'Quest Create!',
                    icon: "tick",
                    intent: Intent.SUCCESS,
                });
            }));
        }));
    };

    updateQuest = () => {
        const {dispatch} = this.props;
        dispatch(updateQuest(this.state.quest, (quest) => {
            this.setState({quest: {...quest, testId: this.props.testId, sort: this.props.index,}});
            dispatch(loadQuestsByTestId(this.state.quest.testId, (test) => {
                AppToaster.show({
                    message: 'Quest Update!',
                    icon: "tick",
                    intent: Intent.SUCCESS,
                });
            }));
        }));
    };

    save = () => {
        if (this.state.quest.id == null) {
            this.createQuest();
        } else {
            this.updateQuest();
        }
    };

    delete = () => {
        if (this.state.quest.id !== null) {
            const {dispatch, removeMethod, index} = this.props;
            dispatch(deleteQuest(this.state.quest, this.state.quest.testId, (quest) => {
                dispatch(loadQuestsByTestId(this.state.quest.testId, (test) => {
                    AppToaster.show({
                        message: 'Quest Delete!',
                        icon: "tick",
                        intent: Intent.SUCCESS,
                    });
                    removeMethod(index);
                }));
            }));
        } else {
            const {removeMethod, index} = this.props;
            removeMethod(index);
        }
    };

    answer = () => {
        const {dispatch, testId, questId} = this.props;

        dispatch(answerResult({testId: testId, questId: questId}, (result) => {
        }));
    };

    onClickSaveHandler = (e) => {
        e.preventDefault();
        this.save();
    };

    onClickDeleteHandler = (e) => {
        e.preventDefault();
        this.delete();
    };

    onSubmitAnswerHandler = (e) => {
        e.preventDefault();
        this.answer();
    };

    onChangeQuestFieldHandler = (e) => {
        const value = e.target.getContent();
        this.setState({
            quest: {
                ...this.state.quest,
                data: {
                    ...this.state.quest.data,
                    quest: value
                }
            }
        });
    };

    onChangePriceFieldHandler = handleNumberChange(value => this.setState(
        {
            quest: {
                ...this.state.quest,
                price: value
            }
        }
    ));
}

export default Query;
