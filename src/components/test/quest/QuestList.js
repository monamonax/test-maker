import React, {Component} from 'react';
import {Menu, Popover, Position} from '@blueprintjs/core';
import * as TestConstants from "../../../constants/test";
import {createQuestModel} from "../../../utils";
import {widgetEditFactory} from "../../../helpers/quest";


class QuestList extends Component {
    static defaultProps = {
        items: [],
    };

    constructor(props) {
        super(props);
        this.state = {
            items: this.props.items,
            currentQuestId: `item-0`,
        };
    };

    addQuest = (type) => {
        let quests = this.state.items.clone();
        let quest = createQuestModel({
            testId: this.props.testId,
            data: {
                quest: "",
            }
        });
        switch (type) {
            case 'string':
                quest.type = TestConstants.TYPE_STRING;
                quest.data.success = '';
                break;
            case 'number':
                quest.type = TestConstants.TYPE_NUMBER;
                quest.data.success = 0;
                break;
            case 'select':
                quest.type = TestConstants.TYPE_ONE_CHECK;
                quest.data.items = [
                    {text: "One", valid: false},
                    {text: "Two", valid: true},
                    {text: "Three", valid: false},
                ];
                break;
            default:
                return;
        }

        quests.push(quest);
        this.setState({
            ...this.state,
            items: quests,
            currentQuestId: `item-${(quests.length - 1)}`,
        });
    };

    removeItem = (index) => {
        let items = this.state.items;
        items.splice(index, 1);

        this.setState({
            items: items,
        });
    };

    onClickAddStringQuestHandler = (e) => {
        e.preventDefault();
        this.addQuest('string');
    };

    onClickAddNumberQuestHandler = (e) => {
        e.preventDefault();
        this.addQuest('number');
    };

    onClickAddSelectQuestHandler = (e) => {
        e.preventDefault();
        this.addQuest('select');
    };
    onClickNothingHandler = (e) => {
        e.preventDefault();
    };

    onClickChangeTabHandler = (navbarTabId) => (e) => {
        e.preventDefault();
        this.setState({currentQuestId: `item-${navbarTabId}`});
    };

    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <div className="card card-default">
                        <div className="card-body">
                            <ul className="nav nav-tabs ml-auto">
                                <Popover content={
                                    <Menu>
                                        <Menu.Item icon="new-text-box" onClick={this.onClickAddStringQuestHandler}
                                                   text="String Type"/>
                                        <Menu.Item icon="numerical" onClick={this.onClickAddNumberQuestHandler}
                                                   text="Number Type"/>
                                        <Menu.Item icon="select" onClick={this.onClickAddSelectQuestHandler}
                                                   text="Select one/more Type"/>
                                    </Menu>
                                } position={Position.BOTTOM}>
                                    <li className="nav-item">
                                        <a className="nav-link active" href="#quest-add"
                                           onClick={this.onClickNothingHandler}><i className="fa fa-plus"/></a>
                                    </li>
                                </Popover>
                                {this.state.items.map((quest, index) => (
                                    <li className="nav-item" key={`item-${index}`}>
                                        <a href={`#quest-${index}`}
                                           className={`item-${index}` === this.state.currentQuestId ? 'nav-link active show' : 'nav-link'}
                                           onClick={this.onClickChangeTabHandler(index)}>
                                            {`Quest #${(index + 1)}`}
                                        </a>
                                    </li>
                                ))}
                            </ul>
                            <div className="tab-content">
                                {this.state.items.map((quest, index) => (
                                    <div className={`item-${index}` === this.state.currentQuestId ? 'tab-pane active' +
                                        ' show' : 'tab-pane'} id={`#quest-${index}`} key={`item-${index}`}>
                                        {widgetEditFactory(quest, index, this.props.editable, this.props.testId, this.removeItem)}
                                    </div>
                                ))}

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default QuestList;
