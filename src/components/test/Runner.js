import React, {Component} from "react";
import * as TestConstants from "../../constants/test";
import {widgetRunFactory} from "../../helpers/quest";
import {Alert, Intent} from "@blueprintjs/core";
import {endResult} from "../../actions";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import {AppToaster} from "../Toaster";

class TestRunner extends Component {
    static defaultProps = {
        test: null,
        result: null
    };

    constructor(props) {
        super(props);

        if (this.props.test.quests.length) {
            this.state = {
                active: this.props.test.quests[0].id,
                endTestConfirmModalIsOpen: false,
            };
        } else {
            this.state = {
                active: null,
                endTestConfirmModalIsOpen: false,
            };
        }
    }


    endTest = () => {
        this.toggleEndTestConfirmModal();
        const {dispatch, history} = this.props;

        dispatch(endResult(this.props.result, () => {
            AppToaster.show({
                message: 'Test end!',
                icon: "tick",
                intent: Intent.SUCCESS,
            });
            history.push(`/tests/${this.props.test.id}/results`);
        }));
    };
    tabClass = (quest) => {
        if (quest.id === this.state.active) {
            return 'nav-link active show';
        }

        return 'nav-link';
    };

    tabActive = (quest) => {
        this.setState({active: quest.id});
    };

    toggleEndTestConfirmModal = () => this.setState({endTestConfirmModalIsOpen: !this.state.endTestConfirmModalIsOpen});

    onClickTabActiveHandler = (quest) => (e) => {
        e.preventDefault();

        this.tabActive(quest);
    };

    onClickEndTestHandler = (e) => {
        e.preventDefault();
        this.toggleEndTestConfirmModal();
    };
    onClickEndTestConfirmHandler = (e) => {
        e.preventDefault();
        this.endTest();
    };

    render() {
        return (
            <div className="row">
                <div className="col-md-3">
                    <div className="card card-primary card-outline">
                        <div className="card-body box-profile">
                            <div className="text-center">
                                <img className="profile-user-img img-fluid img-circle"
                                     src={this.props.result.user.imageUrl} alt={this.props.result.user.name}/>
                            </div>

                            <h3 className="profile-username text-center">{this.props.result.user.name}</h3>

                            <ul className="list-group list-group-unbordered mb-3">
                                <li className="list-group-item">
                                    <b>Group</b> <a className="float-right">{this.props.result.user.group.name}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="card card-primary">
                        <div className="card-header">
                            <h3 className="card-title">Actions</h3>
                        </div>
                        <div className="card-body">
                            {
                                !this.props.result.is_end ?
                                    <>
                                        <button type="button" className="btn btn-success btn-flat"
                                                onClick={this.onClickEndTestHandler}>End test
                                        </button>
                                        <Alert
                                            cancelButtonText="Cancel"
                                            confirmButtonText="Yep"
                                            icon="confirm"
                                            intent={Intent.WARNING}
                                            isOpen={this.state.endTestConfirmModalIsOpen}
                                            onCancel={this.onClickEndTestHandler}
                                            onConfirm={this.onClickEndTestConfirmHandler}
                                        >
                                            <p> Are you sure you want to end the test? </p>
                                        </Alert>
                                    </> : <span>Nothing</span>
                            }
                        </div>
                    </div>
                </div>

                <div className="col-md-9">
                    {this.props.test.status === TestConstants.STATUS_RUNNING && !this.props.result.is_end ? (
                        <div className="card">
                            <div className="card-header p-2">
                                <ul className="nav nav-pills">
                                    {this.props.test.quests.map((quest, index) => (
                                        <li className="nav-item" key={`tab-p-${index}`}>
                                            <a href={`#tab-${index}`} className={this.tabClass(quest)}
                                               onClick={this.onClickTabActiveHandler(quest)}>Quest {index + 1}</a>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                            <div className="card-body">
                                <div className="tab-content">
                                    {this.props.test.quests.map((quest, index) => {
                                        if (quest.id === this.state.active) {
                                            return (
                                                <div className="tab-pane active show" key={`tab-${index}`}
                                                     id={`#tab-${index}`}>
                                                    {widgetRunFactory(quest, index, this.props.result, this.props.test.id)}
                                                </div>
                                            );
                                        }
                                        return (<div key={`tab-${index}`}/>);
                                    })}
                                </div>
                            </div>
                        </div>) : (<div className="card card-outline card-danger">
                        <div className="card-header p-2">
                            <h2>Testing prohibited</h2>
                        </div>
                    </div>)}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {};
};

export default withRouter(connect(mapStateToProps)(TestRunner));
