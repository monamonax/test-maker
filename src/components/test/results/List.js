import React, {Component} from "react";
import ResultsListItem from "./ListItem";

class ResultsList extends Component {
    static defaultProps = {
        items: [],
        test: null,
    };

    render() {
        return (
            <div className="table-responsive">
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th style={{width: 10 + 'px'}}>#</th>
                        <th style={{width: 10 + 'px'}}/>
                        <th>Group</th>
                        <th>User</th>
                        <th style={{width: 10 + 'px'}}>Is End?</th>
                        <th style={{width: 10 + 'px'}}>Balls</th>
                    </tr>
                    </thead>
                    {this.props.items.map(result => (
                        <ResultsListItem test={this.props.test} key={result.id} result={result}/>
                    ))}
                </table>
            </div>
        );
    }
}

export default ResultsList;
