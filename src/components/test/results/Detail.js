import React, {Component} from "react";
import {widgetViewFactory} from "../../../helpers/quest";
import {createBallBadge} from "../../../helpers/test";
import Moment from "react-moment";

class ResultsDetail extends Component {
    static defaultProps = {
        test: null,
        results: null,
    };

    constructor(props) {
        super(props);

        if (this.props.test.quests.length) {
            this.state = {
                active: this.props.test.quests[0].id,
            };
        } else {
            this.state = {
                active: null
            };
        }
    }

    tabClass = (quest) => {
        if (quest.id === this.state.active) {
            return 'nav-link active show';
        }

        return 'nav-link';
    };

    tabActive = (quest) => {
        this.setState({active: quest.id});
    };

    onClickTabActiveHandler = (quest) => (e) => {
        e.preventDefault();

        this.tabActive(quest);
    };

    render() {
        return (
            <div className="row" key={this.props.result.id}>
                <div className="col-md-3">
                    <div className="card card-primary card-outline">
                        <div className="card-body box-profile">
                            <div className="text-center">
                                <img className="profile-user-img img-fluid img-circle"
                                     src={this.props.result.user.imageUrl} alt={this.props.result.user.name}/>
                            </div>

                            <h3 className="profile-username text-center">{this.props.result.user.name}</h3>

                            <ul className="list-group list-group-unbordered mb-3">
                                <li className="list-group-item">
                                    <b>Group</b> <a className="float-right">{this.props.result.user.group.name}</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="card card-primary">
                        <div className="card-header">
                            <h3 className="card-title">Result</h3>
                        </div>
                        <div className="card-body">
                            <strong><i
                                className="fa fa-book mr-1"/> Balls: {createBallBadge(this.props.result.balls, this.props.test.maxPrice)}
                            </strong>
                            <hr/>
                            <strong><i className="fa fa-book mr-1"/> Max Balls: {this.props.test.maxPrice}</strong>
                            <hr/>
                            <strong><i className="fa fa-book mr-1"/> End: </strong>
                            <p className="text-muted">
                                {this.props.result.is_end ? <Moment toNow>{this.props.result.updateAt}</Moment> :
                                    <span>NOT END</span>}
                            </p>
                        </div>
                    </div>
                </div>

                <div className="col-md-9">
                    <div className="card">
                        <div className="card-header p-2">
                            <ul className="nav nav-pills">
                                {this.props.test.quests.map((quest, index) => (
                                    <li className="nav-item" key={`tab-p-${index}`}>
                                        <a href={`#tab-${index}`} className={this.tabClass(quest)}
                                           onClick={this.onClickTabActiveHandler(quest)}>Quest {index + 1}</a>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <div className="card-body">
                            <div className="tab-content">
                                {this.props.test.quests.map((quest, index) => {
                                    if (quest.id === this.state.active) {
                                        return (
                                            <div className="tab-pane active show" key={`tab-${index}`}
                                                 id={`#tab-${index}`}>
                                                {widgetViewFactory(quest, index, this.props.result, this.props.test.id, this.props.test.maxPrice)}
                                            </div>
                                        );
                                    }
                                    return (<div key={`tab-${index}`}/>);
                                })}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ResultsDetail;
