import React, {Component} from "react";
import 'moment-timezone';
import connect from "react-redux/es/connect/connect";
import {withRouter} from 'react-router-dom';
import {createBallBadge, createBoolBadge} from "../../../helpers/test";
import ResultsDetail from "./Detail";

class ResultsListItem extends Component {
    static defaultProps = {
        test: null,
        result: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
        }
    };

    open = () => {
        this.setState({isOpen: !this.state.isOpen});
    };

    onClickOpenHandler = (e) => {
        e.preventDefault();
        this.open();
    };

    render() {
        return (
            <>
                <tbody>
                <tr>
                    <td>{this.props.result.id}</td>
                    <td>
                        <button className="btn btn-sm" onClick={this.onClickOpenHandler}>{this.state.isOpen ?
                            <i className="fa fa-chevron-up"/> : <i className="fa fa-chevron-down"/>}</button>
                    </td>
                    <td>{this.props.result.user.group.name}</td>
                    <td>{this.props.result.user.name}</td>
                    <td>{createBoolBadge(this.props.result.is_end)}</td>
                    <td>{createBallBadge(this.props.result.balls, this.props.test.maxPrice)}</td>
                </tr>
                </tbody>
                {this.state.isOpen && <tbody>
                <tr>
                    <td colSpan={6}>
                        <ResultsDetail test={this.props.test} result={this.props.result}/>
                    </td>
                </tr>
                </tbody>}

            </>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
    };
};

export default withRouter(connect(mapStateToProps)(ResultsListItem));
