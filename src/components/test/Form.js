import React, {Component} from "react";
import {InputGroup, TextArea} from '@blueprintjs/core';
import 'moment-timezone';
import connect from "react-redux/es/connect/connect";
import {handleStringChange} from "../../utils";
import {Link, Redirect} from 'react-router-dom';

import {changeStatusTest} from "../../actions";
import * as TestConstants from "../../constants/test";
import {createStatusTabByStatusId, isAvailabilityAction} from "../../helpers/test";
import QuestList from "./quest/QuestList";

class TestEditForm extends Component {
    static defaultProps = {
        test: {
            id: null,
            title: "",
            description: "",
            quests: [],
        },
        editable: true,
    };

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            test: props.test,
            active: 'main'
        }
    };

    tabClass = (tab) => {
        if (tab === this.state.active) {
            return 'nav-link active show';
        }

        return 'nav-link';
    };

    tabActive = (tab) => {
        this.setState({active: tab});
    };

    submitForm = () => {
        const {dispatch, submit} = this.props;
        dispatch(submit(this.state.test, (test) => {
            if (test.id && this.state.test.id == null) {
                this.setState({test: test});
                this.setState({redirect: true});
            } else {
                this.setState({test: test});
            }

        }));
    };

    changeStatus = (statusId) => {
        const {dispatch} = this.props;
        dispatch(changeStatusTest(this.state.test.id, statusId, (test) => {
            this.setState({test: test});
        }));
    };

    onSubmitFormHandler = (e) => {
        e.preventDefault();
        this.submitForm();
    };
    onClickTabActiveHandler = (tab) => (e) => {
        e.preventDefault();

        this.tabActive(tab);
    };
    onChangeTitleFieldHandler = handleStringChange(value => this.setState({test: {...this.state.test, title: value}}));
    onChangeDescriptionFieldHandler = handleStringChange(value => this.setState({
        test: {
            ...this.state.test,
            description: value
        }
    }));
    onClickChangeStatusHandler = (statusId, action) => (e) => {
        e.preventDefault();
        if (action) {
            if (isAvailabilityAction(action, this.state.test)) {
                this.changeStatus(statusId);
            }
        } else {
            this.changeStatus(statusId);
        }
    };

    renderActive() {
        switch (this.state.active) {
            case 'quests':
                return (
                    <QuestList items={this.state.test.quests} editable={this.props.editable}
                               testId={this.state.test.id}/>
                );
            case 'main':
                return (
                    <form onSubmit={this.onSubmitFormHandler}>
                        <div className="form-group">
                            <label htmlFor="testTitle">Title</label>
                            <InputGroup
                                type="text"
                                disabled={!this.props.editable}
                                name="title"
                                id="testTitle"
                                placeholder="Enter title test"
                                value={this.state.test.title}
                                onChange={this.onChangeTitleFieldHandler}
                                required={true}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="testDescription">Description</label>
                            <TextArea
                                disabled={!this.props.editable}
                                className=".bp3-fill form-control"
                                id="testDescription"
                                large={true}
                                onChange={this.onChangeDescriptionFieldHandler}
                                value={this.state.test.description}
                            />
                        </div>

                        <div className="btn-group" role="group" aria-label="Actions">
                            <button disabled={!this.props.editable} className="btn btn-success btn-flat"
                                    type="submit">Save only Main
                                Data
                            </button>
                        </div>
                        <hr/>
                    </form>
                );
            default:
                return (<></>);
        }

        return (<></>);
    }

    render() {
        if (this.state.redirect) {
            return (<Redirect to={{pathname: `/tests/${this.state.test.id}/update`}}/>);
        }
        return (
            <div className="row">
                {
                    this.props.test.id && <div className="col-md-3">
                        <div className="card card-primary card-outline">
                            <div className="card-header">
                                <h3 className="card-title">Actions</h3>
                            </div>
                            <div className="card-body">
                                <h5 className="mb-1">Current
                                    status: {createStatusTabByStatusId(this.state.test.status)}</h5>
                                <hr/>
                                <div className="btn-group btn-flat btn-group-sm">
                                    {
                                        isAvailabilityAction('view', this.props.test) ?
                                            <Link to={`/tests/${this.props.test.id}/view`}
                                                  className="btn btn-primary btn-flat"
                                                  disabled={!isAvailabilityAction('view', this.props.test)}>View</Link>
                                            :
                                            <button type="button" className="btn btn-primary btn-flat"
                                                    disabled={!isAvailabilityAction('view', this.props.test)}>View</button>
                                    }
                                    {
                                        isAvailabilityAction('ready', this.props.test) &&
                                        <button type="button" className="btn btn-success btn-flat"
                                                onClick={this.onClickChangeStatusHandler(TestConstants.STATUS_READY_RUN, 'ready')}>Ready
                                            to run</button>
                                    }
                                    {
                                        isAvailabilityAction('run', this.props.test) &&
                                        <button type="button" className="btn btn-success btn-flat"
                                                onClick={this.onClickChangeStatusHandler(TestConstants.STATUS_RUNNING, 'run')}>Run</button>
                                    }
                                    {
                                        isAvailabilityAction('end', this.props.test) &&
                                        <button type="button" className="btn btn-success btn-flat"
                                                onClick={this.onClickChangeStatusHandler(TestConstants.STATUS_END, 'end')}>End</button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                }
                <div className={this.props.test.id ? "col-9" : "col-12"}>
                    <div className="card card-primary card-outline">
                        {this.props.test.id ? <div className="card-header p-2">
                            <ul className="nav nav-pills">
                                <li className="nav-item">
                                    <a href="#tab-main" className={this.tabClass('main')}
                                       onClick={this.onClickTabActiveHandler('main')}>Main Data</a>
                                </li>
                                <li className="nav-item">
                                    <a href="#tab-quests" className={this.tabClass('quests')}
                                       onClick={this.onClickTabActiveHandler('quests')}>Questions</a>
                                </li>
                            </ul>
                        </div> : <></>}

                        <div className="card-body">
                            <div className="tab-content">
                                {this.renderActive()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({});

export default connect(mapStateToProps)(TestEditForm);
