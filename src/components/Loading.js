import React, {Component} from 'react';
import {Spinner} from '@blueprintjs/core';

class Loading extends Component {
    render() {
        return (
            <div id="notfound">
                <div className="notfound">
                    <div className="notfound-404">
                        <Spinner intent={"primary"} size="100"/>
                    </div>
                    <div className="text-center pb-5">
                        <img src="/images/logo.png" alt="Test Maker" className="img-fluid"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Loading;
