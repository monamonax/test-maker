let url = "http://localhost:8040/api";
if (process.env.NODE_ENV === 'production') {
    url = "http://tm.mmx-studio.ru/api";
}

export const API_URL = url;
export const ENDPOINT_USERS = "/users";
export const ENDPOINT_GROUPS = "/user_groups";
export const ENDPOINT_TEST = "/tests";
export const ENDPOINT_QUEST = "/quests";
export const ENDPOINT_RESULT = "/results";
