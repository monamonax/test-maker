export const TYPE_STRING = 1;
export const TYPE_MORE_CHECK = 2;
export const TYPE_ONE_CHECK = 3;
export const TYPE_NUMBER = 4;

export const STATUS_NEW = 1;
export const STATUS_READY_RUN = 2;
export const STATUS_RUNNING = 5;
export const STATUS_END = 6;
export const STATUS_DELETE = 9;
