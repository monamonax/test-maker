import * as ActionTypes from "../../constants/action-types";

import {groupList} from "./list";

export default function (state = {isFetching: false, didInvalidate: false, items: []}, action) {
    switch (action.type) {
        case ActionTypes.GROUP_LIST_REQUEST:
        case ActionTypes.GROUP_LIST_RECEIVE:
            return {
                ...state,
                ...groupList(state, action)
            };
        default:
            return state;
    }
}

