import * as ActionTypes from "../../constants/action-types";

export const groupList = (state = {isFetching: false, didInvalidate: false, items: []}, action) => {
    switch (action.type) {
        case ActionTypes.GROUP_LIST_REQUEST:
            return {
                ...state,
                isFetching: true,
                didInvalidate: false
            };
        case ActionTypes.GROUP_LIST_RECEIVE:
            return {
                ...state,
                isFetching: false,
                didInvalidate: false,
                items: action.payload.groups,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
