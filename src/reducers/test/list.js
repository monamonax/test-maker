import * as ActionTypes from "../../constants/action-types";

export const testList = (state, action) => {
    switch (action.type) {
        case ActionTypes.TEST_LIST_UPDATE:
            return {
                ...state,
                isFetching: false,
                didUpdate: true
            };
        case ActionTypes.TEST_LIST_REQUEST:
            return {
                ...state,
                isFetching: true,
                didUpdate: false
            };
        case ActionTypes.TEST_LIST_RECEIVE:
            return {
                ...state,
                isFetching: false,
                didUpdate: false,
                items: action.payload.tests,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
