import * as ActionTypes from "../../constants/action-types";
import {testList} from './list';
import {changeStatusTest} from './changeStatus';
import {createTest} from './create';
import {deleteTest} from './delete';
import {updateTest} from './update';
import {createQuest, deleteQuest, updateQuest} from './quest'
import {answerResult, createResult, endResult} from './results'

const initialState = {
    list: {
        isFetching: false,
        didUpdate: true,
        items: [],
    },
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ActionTypes.TEST_LIST_UPDATE:
        case ActionTypes.TEST_LIST_REQUEST:
        case ActionTypes.TEST_LIST_RECEIVE:
            return {
                ...state,
                list: testList(state.list, action)
            };
        case ActionTypes.TEST_CREATE_REQUEST:
        case ActionTypes.TEST_CREATE_RECEIVE:
            return {
                ...state,
                list: createTest(state.list, action)
            };
        case ActionTypes.TEST_UPDATE_REQUEST:
        case ActionTypes.TEST_UPDATE_RECEIVE:
            return {
                ...state,
                list: updateTest(state.list, action)
            };
        case ActionTypes.TEST_CHANGE_STATUS_REQUEST:
        case ActionTypes.TEST_CHANGE_STATUS_RECEIVE:
            return {
                ...state,
                list: changeStatusTest(state.list, action)
            };
        case ActionTypes.TEST_DELETE_REQUEST:
        case ActionTypes.TEST_DELETE_RECEIVE:
            return {
                ...state,
                list: deleteTest(state.list, action)
            };
        // case ActionTypes.QUEST_LIST_UPDATE:
        // case ActionTypes.QUEST_LIST_REQUEST:
        // case ActionTypes.QUEST_LIST_RECEIVE:
        //     return {
        //         ...state,
        //         list: questList(state.list, action)
        //     };
        case ActionTypes.QUEST_CREATE_REQUEST:
        case ActionTypes.QUEST_CREATE_RECEIVE:
            return {
                ...state,
                list: createQuest(state.list, action)
            };
        case ActionTypes.QUEST_UPDATE_REQUEST:
        case ActionTypes.QUEST_UPDATE_RECEIVE:
            return {
                ...state,
                list: updateQuest(state.list, action)
            };
        case ActionTypes.QUEST_DELETE_REQUEST:
        case ActionTypes.QUEST_DELETE_RECEIVE:
            return {
                ...state,
                list: deleteQuest(state.list, action)
            };
        case ActionTypes.RESULT_CREATE_REQUEST:
        case ActionTypes.RESULT_CREATE_RECEIVE:
            return {
                ...state,
                list: createResult(state.list, action)
            };
        case ActionTypes.RESULT_ANSWER_REQUEST:
        case ActionTypes.RESULT_ANSWER_RECEIVE:
            return {
                ...state,
                list: answerResult(state.list, action)
            };
        case ActionTypes.RESULT_END_REQUEST:
        case ActionTypes.RESULT_END_RECEIVE:
            return {
                ...state,
                list: endResult(state.list, action)
            };
        default:
            return state;
    }
}
