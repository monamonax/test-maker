import * as ActionTypes from "../../constants/action-types";
import {createTestModel} from "../../utils";

var _ = require('lodash');

export const changeStatusTest = (state, action) => {
    switch (action.type) {
        case ActionTypes.TEST_CHANGE_STATUS_RECEIVE:
            let items = state.items.clone();
            const index = _.findIndex(items, (test) => (test.id === action.payload.testId));
            items[index] = (createTestModel(action.payload.test));
            return {
                ...state,
                items: items,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
