import * as ActionTypes from "../../constants/action-types";
import {createTestModel} from "../../utils";

export const createTest = (state, action) => {
    switch (action.type) {
        case ActionTypes.TEST_CREATE_RECEIVE:
            const items = state.items.clone();
            items.push(createTestModel(action.payload.test));
            return {
                ...state,
                items: items,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
