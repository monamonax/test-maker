import {createResult} from './create';
import {answerResult} from './answer';
import {endResult} from './end';

export {createResult, answerResult, endResult};
