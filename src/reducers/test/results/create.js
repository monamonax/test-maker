import * as ActionTypes from "../../../constants/action-types";

var _ = require('lodash');

export const createResult = (state, action) => {
    switch (action.type) {
        case ActionTypes.RESULT_CREATE_RECEIVE:
            const items = state.items.clone();
            const index = _.findIndex(items, (test) => (test.id === action.payload.testId));
            items[index].results.push(action.payload.result);
            return {
                ...state,
                items: items,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
