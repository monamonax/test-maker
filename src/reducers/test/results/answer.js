import * as ActionTypes from "../../../constants/action-types";

var _ = require('lodash');

export const answerResult = (state, action) => {
    switch (action.type) {
        case ActionTypes.RESULT_ANSWER_RECEIVE:
            const items = state.items.clone();
            const index = _.findIndex(items, (test) => (test.id === action.payload.testId));
            const indexResult = _.findIndex(items[index].results, (result) => (result.id === action.payload.result.id));
            items[index].results[indexResult] = action.payload.result;
            return {
                ...state,
                items: items,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
