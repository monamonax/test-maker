import * as ActionTypes from "../../constants/action-types";
import {createTestModel} from "../../utils";

var _ = require('lodash');

export const deleteTest = (state, action) => {
    switch (action.type) {
        case ActionTypes.TEST_DELETE_RECEIVE:
            let items = state.items.clone();
            const index = _.findIndex(items, (test) => (test.id === action.payload.test.id));
            items[index] = createTestModel(action.payload.test);
            return {
                ...state,
                items: items,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
