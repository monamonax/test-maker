import * as ActionTypes from "../../../constants/action-types";
import {createQuestModel} from "../../../utils";

var _ = require('lodash');

export const updateQuest = (state, action) => {
    switch (action.type) {
        case ActionTypes.QUEST_CREATE_RECEIVE:
            const items = state.items.clone();
            const index = _.findIndex(items, (test) => (test.id === action.payload.testId));
            const indexQuest = _.findIndex(items[index].quests, (quest) => (quest.id === action.payload.quest.id));
            items[index].quests[indexQuest] = createQuestModel(action.payload.quest);
            return {
                ...state,
                items: items,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
