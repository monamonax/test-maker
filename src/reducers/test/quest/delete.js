import * as ActionTypes from "../../../constants/action-types";

var _ = require('lodash');

export const deleteQuest = (state, action) => {
    switch (action.type) {
        case ActionTypes.QUEST_DELETE_RECEIVE:
            const items = state.items.clone();
            const index = _.findIndex(items, (test) => (test.id === action.payload.testId));
            if (typeof items[index] !== "undefined") {
                const questIndex = _.findIndex(items[index].quests, (quest) => (quest.id === action.payload.quest.id));
                items[index].quests.splice(questIndex, 1);
            }
            return {
                ...state,
                items: items,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
