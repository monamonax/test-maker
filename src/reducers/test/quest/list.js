import * as ActionTypes from "../../../constants/action-types";
import {createQuestModel} from "../../../utils";

var _ = require('lodash');

export const questList = (state, action) => {
    switch (action.type) {
        case ActionTypes.QUEST_LIST_RECEIVE:
            let quests = [];
            action.payload.quests.forEach((quest) => {
                quests.push(createQuestModel(quest));
            });

            let items = state.items.clone();
            const index = _.findIndex(items, (test) => (test.id === action.payload.testId));
            if (index !== -1) {
                items[index].quests = quests
            }

            return {
                ...state,
                items: items
            };
        default:
            return state;
    }
};
