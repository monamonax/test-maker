import * as ActionTypes from '../constants/action-types';
import {combineReducers} from 'redux';
import TestReducers from "./test";
import UserReducers from "./user";
import GroupReducers from "./groups";
import PageReducers from "./page";

const errorMessage = (state = null, action) => {
    const {type, error} = action;

    if (type === ActionTypes.RESET_ERROR_MESSAGE) {
        return null;
    } else if (error) {
        return error;
    }

    return state;
};

const rootReducer = combineReducers({
    group: GroupReducers,
    user: UserReducers,
    test: TestReducers,
    errorMessage,
    page: PageReducers,
});

export default rootReducer;
