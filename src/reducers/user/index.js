import * as ActionTypes from "../../constants/action-types";

import {userList} from './list';
import {userGet} from './get';
import {authUser} from './auth';
import {getLocalStorageByKey} from "../../utils";

const getAuthUser = () => {
    const user = getLocalStorageByKey('user', null);
    if (typeof user === 'string') {
        return JSON.parse(user);
    }

    return user;
};

const initialState = {
    isAuth: false,
    user: getAuthUser(),
    list: {
        isFetching: false,
        didUpdate: false,
        items: [],
    },
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ActionTypes.USER_LIST_REQUEST:
        case ActionTypes.USER_LIST_RECEIVE:
            return {
                ...state,
                list: userList(state.list, action)
            };
        case ActionTypes.USER_GET_RECEIVE:
            return {
                ...state,
                list: userGet(state.list, action)
            };
        case ActionTypes.USER_AUTH_BEGIN:
        case ActionTypes.USER_AUTH_END:
        case ActionTypes.USER_LOGOUT:
            return {
                ...state,
                ...authUser(state, action)
            };
        default:
            return state;
    }
}
