import * as ActionTypes from "../../constants/action-types";
import {createUserModel} from "../../utils";

var _ = require('lodash');

export const userGet = (state = {isFetching: false, didUpdate: false, items: []}, action) => {
    switch (action.type) {
        case ActionTypes.USER_GET_RECEIVE:
            const items = state.items.clone();
            const index = _.findIndex(items, (user) => (user.id === action.payload.user.id));
            items[index] = createUserModel(action.payload.user);
            return {
                ...state,
                isFetching: false,
                didUpdate: false,
                items: items,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
