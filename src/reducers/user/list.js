import * as ActionTypes from "../../constants/action-types";

export const userList = (state = {isFetching: false, didUpdate: false, items: []}, action) => {
    switch (action.type) {
        case ActionTypes.USER_LIST_REQUEST:
            return {
                ...state,
                isFetching: true,
                didUpdate: false
            };
        case ActionTypes.USER_LIST_RECEIVE:
            return {
                ...state,
                isFetching: false,
                didUpdate: false,
                items: action.payload.users,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
