import * as ActionTypes from "../../constants/action-types";
import {removeLocalStorageByKey, setLocalStorageByKey} from "../../utils";

export const authUser = (state, action) => {
    switch (action.type) {
        case ActionTypes.USER_AUTH_END:
            setLocalStorageByKey('user', JSON.stringify(action.payload.user));
            return {
                ...state,
                isAuth: action.payload.isAuth,
                user: action.payload.user
            };
        case ActionTypes.USER_LOGOUT:
            removeLocalStorageByKey('user');
            return {
                ...state,
                user: null,
                isAuth: false,
            };
        default:
            return state;
    }
};
