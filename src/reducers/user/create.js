import * as ActionTypes from "../../constants/action-types";
import {createUserModel} from "../../utils";

var _ = require('lodash');

export const createUser = (state = {isFetching: false, didInvalidate: false, items: []}, action) => {
    switch (action.type) {
        case ActionTypes.USER_CREATE_RECEIVE:
            const items = state.items.clone();
            items.push(createUserModel(action.payload.user));
            return {
                ...state,
                items: items,
                lastUpdated: action.payload.receivedAt
            };
        default:
            return state;
    }
};
