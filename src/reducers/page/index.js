import * as ActionTypes from "../../constants/action-types";
import {getSidebarOpenStatus, setSidebarOpenStatus} from "../../utils";

const initialState = {
    sidebar_open: getSidebarOpenStatus(),
};

export const page = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.PAGE_SIDEBAR_COLLAPSE:
            setSidebarOpenStatus(action.payload.sidebar_open);
            return {
                ...state,
                sidebar_open: action.payload.sidebar_open,
            };
        default:
            return state;
    }
};

export default function (state = initialState, action) {
    switch (action.type) {
        case ActionTypes.PAGE_SIDEBAR_COLLAPSE:
            return {
                ...state,
                ...page(state, action)
            };
        default:
            return state;
    }
}

