import * as TestConstants from "../constants/test";
import {
    NumberQuestEdit,
    NumberQuestRun,
    NumberQuestView,
    SelectQuestEdit,
    SelectQuestRun,
    SelectQuestView,
    StringQuestEdit,
    StringQuestRun,
    StringQuestView
} from "../components/test/quest";
import React from "react";

export const widgetEditFactory = (quest, index, editable, testId, removeMethod) => {
    switch (quest.type) {
        case TestConstants.TYPE_STRING:
            return (<StringQuestEdit quest={quest} index={index} editable={editable} testId={testId}
                                     removeMethod={removeMethod}/>);
        case TestConstants.TYPE_NUMBER:
            return (<NumberQuestEdit quest={quest} index={index} editable={editable} testId={testId}
                                     removeMethod={removeMethod}/>);
        case TestConstants.TYPE_ONE_CHECK:
        case TestConstants.TYPE_MORE_CHECK:
            return (<SelectQuestEdit quest={quest} index={index} editable={editable} testId={testId}
                                     removeMethod={removeMethod}/>);
        default:
            return (<div key={quest.id}/>);
    }
};

export const widgetViewFactory = (quest, index, result, testId, maxPrice = 0) => {
    switch (quest.type) {
        case TestConstants.TYPE_STRING:
            return (<StringQuestView quest={quest} index={index} result={result} testId={testId} maxPrice={maxPrice}/>);
        case TestConstants.TYPE_NUMBER:
            return (<NumberQuestView quest={quest} index={index} result={result} testId={testId} maxPrice={maxPrice}/>);
        case TestConstants.TYPE_ONE_CHECK:
        case TestConstants.TYPE_MORE_CHECK:
            return (<SelectQuestView quest={quest} index={index} result={result} testId={testId} maxPrice={maxPrice}/>);
        default:
            return (<div key={quest.id}/>);
    }
};

export const widgetRunFactory = (quest, index, result, testId) => {
    switch (quest.type) {
        case TestConstants.TYPE_STRING:
            return (<StringQuestRun quest={quest} index={index} result={result} testId={testId}/>);
        case TestConstants.TYPE_NUMBER:
            return (<NumberQuestRun quest={quest} index={index} result={result} testId={testId}/>);
        case TestConstants.TYPE_ONE_CHECK:
        case TestConstants.TYPE_MORE_CHECK:
            return (<SelectQuestRun quest={quest} index={index} result={result} testId={testId}/>);
        default:
            return (<div key={quest.id}/>);
    }
};
