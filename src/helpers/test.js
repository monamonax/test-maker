import * as TestConstants from "../constants/test";
import Moment from "react-moment";
import React from "react";

var _ = require('lodash');

export const getTypeTextByTypeId = (questTypeId) => {
    switch (questTypeId) {
        case TestConstants.TYPE_STRING:
            return "String Type";
        case TestConstants.TYPE_NUMBER:
            return "Number Type";
        case TestConstants.TYPE_ONE_CHECK:
        case TestConstants.TYPE_MORE_CHECK:
            return "Select one/more Type";
        default:
            return "Fucking type";
    }
};

export const createTimeBlock = (test) => {
    switch (test.status) {
        case TestConstants.STATUS_NEW:
            return (<small>Create Time: <Moment toNow>{test.createAt}</Moment></small>);
        case TestConstants.STATUS_READY_RUN:
            return (<small>Update Time: <Moment toNow>{test.updateAt}</Moment></small>);
        case TestConstants.STATUS_RUNNING:
            return (<small>Update Time: <Moment toNow>{test.startAt}</Moment></small>);
        case TestConstants.STATUS_END:
            return (<small>Update Time: <Moment toNow>{test.endAt}</Moment></small>);
        case TestConstants.STATUS_DELETE:
            return (<small>Delete Time: <Moment toNow>{test.updateAt}</Moment></small>);
        default:
            return (<small>Create Time: <Moment toNow>{test.createAt}</Moment></small>);
    }
};

export const createStatusTabByStatusId = (statusId) => {
    switch (statusId) {
        case TestConstants.STATUS_NEW:
            return (<span className="badge bg-gray">{'New'}</span>);
        case TestConstants.STATUS_READY_RUN:
            return (<span className="badge bg-success">{'Ready to run'}</span>);
        case TestConstants.STATUS_RUNNING:
            return (<span className="badge bg-warning">{'Running'}</span>);
        case TestConstants.STATUS_END:
            return (<span className="badge bg-primary">{'End'}</span>);
        case TestConstants.STATUS_DELETE:
            return (<span className="badge bg-danger">{'Delete'}</span>);
        default :
            return (<span className="badge bg-aqua">{'New'}</span>);
    }
};

export const createBallBadge = (bal, max) => {
    const proc = bal / max * 100;
    if (proc > 90) {
        return (<span className="badge bg-success">{bal}</span>);
    }
    if (proc > 70) {
        return (<span className="badge bg-warning">{bal}</span>);
    }

    if (proc > 30) {
        return (<span className="badge bg-gray">{bal}</span>);
    }

    return (<span className="badge bg-danger">{bal}</span>);
};

export const createBoolBadge = (value) => {
    if (value) {
        return (<span className="badge bg-success">Yes</span>);
    }

    return (<span className="badge bg-danger">No</span>);
};

export const isAvailabilityAction = (action, test) => {
    switch (test.status) {
        case TestConstants.STATUS_NEW:
            switch (action) {
                case 'view':
                case 'ready':
                case 'edit':
                case 'delete':
                    return true;
                default:
                    return false;
            }
        case TestConstants.STATUS_READY_RUN:
            switch (action) {
                case 'view':
                case 'edit':
                case 'run':
                case 'delete':
                    return true;
                default:
                    return false;
            }
        case TestConstants.STATUS_RUNNING:
            switch (action) {
                case 'view':
                case 'end':
                    return true;
                default:
                    return false;
            }
        case TestConstants.STATUS_END:
            switch (action) {
                case 'view':
                    return true;
                default:
                    return false;
            }
        case TestConstants.STATUS_DELETE:
            switch (action) {
                default:
                    return false;
            }
        default:
            return false;
    }
};

export const getResultByUserId = (test, userId) => {
    const result = test.results[_.findIndex(test.results, (result) => (parseInt(result.userId) === parseInt(userId)))];

    return result ? result : null;
};
