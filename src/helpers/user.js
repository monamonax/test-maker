export const can = (user, onlyAdmin = true) => {
    return onlyAdmin && user.isAdmin === true;
};
