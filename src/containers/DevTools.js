import React from 'react'
import {createDevTools} from 'redux-devtools'
import LogMonitor from 'redux-devtools-log-monitor'
import DockMonitor from 'redux-devtools-dock-monitor'

const visible = process.env.NODE_ENV !== 'production';
export default createDevTools(
    <DockMonitor toggleVisibilityKey="ctrl-h" changePositionKey="ctrl-w" defaultIsVisible={visible}>
        <LogMonitor/>
    </DockMonitor>
);
