import React, {Component} from 'react'
import connect from "react-redux/es/connect/connect";
import {Content, Footer, Header} from "../../components";
import {loadUsers} from "../../actions";
import {Link} from "react-router-dom";

class PageUsers extends Component {
    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(loadUsers());
    }

    render() {
        return (
            <div>
                <Header/>
                <Content>
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0 text-dark">Users</h1>
                                </div>
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                        <li className="breadcrumb-item active">Users</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="content">
                        <div className="container-fluid">
                            Coming soon
                        </div>
                    </div>
                </Content>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    errorMessage: state.errorMessage,
    test: state.test,
    user: state.user,
    group: state.group
});

export default connect(mapStateToProps)(PageUsers);
