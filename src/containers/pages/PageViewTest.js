import React, {Component} from 'react';
import {Content, Footer, Header} from "../../components";
import connect from "react-redux/es/connect/connect";
import {Link, withRouter} from 'react-router-dom';
import {loadTests} from "../../actions";

var _ = require('lodash');

class PageViewTest extends Component {
    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(loadTests(() => {
        }));
    }

    render() {
        return (
            <div>
                <Header/>
                <Content>
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0 text-dark">View test #{this.props.testId}</h1>
                                </div>
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                        <li className="breadcrumb-item"><Link to="/tests">Tests</Link></li>
                                        <li className="breadcrumb-item active">View test #{this.props.testId}</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="content">
                        <div className="container-fluid">
                            Coming soon
                        </div>
                    </div>
                </Content>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        testId: parseInt(ownProps.match.params.testId),
        test: state.test.list.items[_.findIndex(state.test.list.items, (test) => (test.id === parseInt(ownProps.match.params.testId)))],
        group: state.group
    };
};

export default withRouter(connect(mapStateToProps)(PageViewTest));
