import React, {Component} from 'react';
import {Content, Footer, Header} from "../../components";

class PageHome extends Component {
    render() {
        return (
            <>
                <Header/>
                <Content>
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0 text-dark">Dashboard</h1>
                                </div>
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item active">Home</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="content">
                        <div className="container-fluid">
                            Coming soon
                        </div>
                    </div>
                </Content>
                <Footer/>
            </>
        );
    }
}

export default PageHome;
