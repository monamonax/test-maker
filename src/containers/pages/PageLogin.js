import React, {Component} from 'react';
import {Link, Redirect, withRouter} from 'react-router-dom';
import connect from "react-redux/es/connect/connect";
import {GoogleLogin} from 'react-google-login';
import {createUserModel} from "../../utils";
import {auth} from "../../actions/user";
import Loading from "../../components/Loading";
import * as config from '../../config';

class PageLogin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectToReferrer: false
        };
    }

    componentDidMount() {
        this.autoLogin();
    }

    autoLogin = () => {
        const {user, dispatch} = this.props;
        if (user.user) {
            dispatch(auth(user.user, (user) => {
                this.setState({redirectToReferrer: true});
            }))
        }
    };

    login = (response) => {
        const user = createUserModel({
            name: response.profileObj.name,
            googleId: response.googleId,
            imageUrl: response.profileObj.imageUrl
        });
        const {dispatch} = this.props;

        dispatch(auth(user, (user) => {
            this.setState({redirectToReferrer: true});
        }));
    };

    fail = (response) => {

    };

    render() {
        let {from} = this.props.location.state || {from: {pathname: "/"}};
        let {redirectToReferrer} = this.state;

        if (redirectToReferrer) return <Redirect to={from}/>;

        return (
            this.props.user.user ?
                <Loading/> :
                <div className="login-box">
                    <div className="login-logo">
                        <Link to="/"><img src="/images/logo.png" alt="Test Maker" className="img-fluid"/></Link>
                    </div>
                    <div className="card">
                        <div className="card-body login-card-body">
                            <p className="login-box-msg">Sign in to start your session</p>

                            <div className="social-auth-links text-center mb-3">
                                <GoogleLogin
                                    clientId={config.default.googleApi}
                                    buttonText="Login"
                                    onSuccess={this.login}
                                    onFailure={this.fail}
                                    render={renderProps => (
                                        <button onClick={renderProps.onClick} className="btn btn-block btn-danger">
                                            <i className="fa fa-google mr-2"/> Sign in using Google
                                        </button>
                                    )}
                                />
                            </div>
                            <p className="mb-1">

                            </p>
                        </div>
                    </div>
                </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user,
    page: state.page,
});

export default withRouter(connect(mapStateToProps)(PageLogin));
