import React, {Component} from 'react';
import {Content, Footer, Header} from "../../components";
import connect from "react-redux/es/connect/connect";
import TestList from "../../components/test/List";
import {Link} from 'react-router-dom';
import {loadTests} from "../../actions";
import {can} from "../../helpers/user";

class PageTests extends Component {
    componentDidMount() {
        const {dispatch, user} = this.props;
        dispatch(loadTests(user.user));
    }

    render() {
        return (
            <div>
                <Header/>
                <Content>
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0 text-dark">Tests</h1>
                                </div>
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                        <li className="breadcrumb-item active">Tests</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="content">
                        <div className="container-fluid">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="card card-primary card-outline">
                                        <div className="card-header">
                                            <h3 className="card-title">Tests list</h3>
                                        </div>
                                        <div className="card-body pad table-responsive">
                                            {
                                                can(this.props.user.user) && <>
                                                    <p>Actions:</p>
                                                    <div className="btn-group" role="group" aria-label="Actions">
                                                        <Link className="btn btn-success btn-flat btn-sm"
                                                              to="/tests/create">Create new test</Link>
                                                    </div>
                                                    <hr/>
                                                </>
                                            }
                                            <TestList items={this.props.test.list.items}/>
                                            {this.props.test.list.isFetching &&
                                            <div className="overlay">
                                                <i className="fa fa-refresh fa-spin"></i>
                                            </div>
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Content>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    test: state.test,
    user: state.user,
    group: state.group
});

export default connect(mapStateToProps)(PageTests);
