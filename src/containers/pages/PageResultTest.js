import React, {Component} from 'react';
import {Content, Footer, Header} from "../../components";
import connect from "react-redux/es/connect/connect";
import {Link, withRouter} from 'react-router-dom';
import {getResultByUserId} from "../../helpers/test";
import {loadTests} from "../../actions/test";
import {Spinner} from "@blueprintjs/core";
import ResultsDetail from "../../components/test/results/Detail";
import ResultsList from "../../components/test/results/List";

var _ = require('lodash');

class PageResultTest extends Component {
    constructor(props) {
        super(props);

        this.state = {
            result: null,
        };

    };

    componentDidMount() {
        const {dispatch, testId, test, user, history} = this.props;
        if (!test) {
            dispatch(loadTests(user.user, () => {
            }));
        } else {
            this.setState({
                result: getResultByUserId(test, user.user.id)
            });
        }
    }

    componentDidUpdate(prevProps) {
        if (typeof prevProps.test === 'undefined' && typeof this.props.test !== 'undefined') {
            this.setState({
                result: getResultByUserId(this.props.test, this.props.user.user.id)
            });
        }
    }

    render() {
        return (
            <div>
                <Header/>
                <Content>
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0 text-dark">Test # {this.props.testId} Results</h1>
                                </div>
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                        <li className="breadcrumb-item"><Link to="/tests">Tests</Link></li>
                                        <li className="breadcrumb-item "><Link to={`/tests/${this.props.testId}`}>Test
                                            view # {this.props.testId}</Link></li>
                                        <li className="breadcrumb-item active">Results</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="content">
                        <div className="container-fluid">
                            {
                                this.props.user.user.isAdmin ?
                                    this.props.test && this.props.test.results &&
                                    <ResultsList items={this.props.test.results} test={this.props.test}/> :
                                    this.props.test && this.props.test.quests && this.state.result ?
                                        <ResultsDetail test={this.props.test} result={this.state.result}/> :
                                        <Spinner intent={"primary"} size="100"/>
                            }
                        </div>
                    </div>
                </Content>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    const test = state.test.list.items[_.findIndex(state.test.list.items, (test) => (test.id === parseInt(ownProps.match.params.testId)))];
    return {
        user: state.user,
        testId: parseInt(ownProps.match.params.testId),
        test: test,
        group: state.group
    };
};

export default withRouter(connect(mapStateToProps)(PageResultTest));
