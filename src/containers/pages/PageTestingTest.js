import React, {Component} from 'react';
import {Content, Footer, Header} from "../../components";
import connect from "react-redux/es/connect/connect";
import {Link, withRouter} from 'react-router-dom';
import {loadTests} from "../../actions/test";
import {getResultByUserId} from "../../helpers/test";
import {createResult} from "../../actions";
import {createResultModel} from "../../utils";
import TestRunner from "../../components/test/Runner";

var _ = require('lodash');

class PageTestingTest extends Component {
    constructor(props) {
        super(props);

        this.state = {
            result: null,
        };

    };

    componentDidMount() {
        const {dispatch, testId, test, user, history} = this.props;
        if (!test) {
            dispatch(loadTests(user.user, (tests) => {
                const test = tests[_.findIndex(tests, (test) => (test.id === parseInt(testId)))];
                if (test) {
                    const result = getResultByUserId(test, user.user.id);
                    if (result !== null) {
                        this.setState({
                            result: result
                        });
                    } else {
                        dispatch(createResult(createResultModel({testId: testId, userId: user.user.id,}, (result) => {
                            this.setState({
                                result: result
                            });
                        })))
                    }
                }
            }));
        } else {
            const result = getResultByUserId(test, user.user.id);
            if (result !== null) {
                this.setState({
                    result: result
                });
            } else {
                dispatch(createResult(createResultModel({testId: testId, userId: user.user.id,}, (result) => {
                    this.setState({
                        result: result
                    });
                })))
            }
        }
    }

    componentDidUpdate(prevProps) {
        if (typeof prevProps.test === 'undefined' && typeof this.props.test !== 'undefined') {
            const result = getResultByUserId(this.props.test, this.props.user.user.id);
            if (result !== null) {
                this.setState({
                    result: result
                });
            }
        } else if (typeof this.props.test !== 'undefined') {
            if (this.state.result === null) {
                const result = getResultByUserId(this.props.test, this.props.user.user.id);
                if (result !== null) {
                    this.setState({
                        result: result
                    });
                }
            }
        }
    }

    render() {
        return (
            <div>
                <Header/>
                <Content>
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0 text-dark">Testing</h1>
                                </div>
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                        <li className="breadcrumb-item"><Link to="/tests">Tests</Link></li>
                                        <li className="breadcrumb-item "><Link to={`/tests/${this.props.testId}`}>Test
                                            view # {this.props.testId}</Link></li>
                                        <li className="breadcrumb-item active">Testing</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="content">
                        <div className="container-fluid">
                            {this.props.test && this.state.result &&
                            <TestRunner test={this.props.test} result={this.state.result}/>}
                        </div>
                    </div>
                </Content>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        testId: parseInt(ownProps.match.params.testId),
        test: state.test.list.items[_.findIndex(state.test.list.items, (test) => (test.id === parseInt(ownProps.match.params.testId)))],
        group: state.group
    };
};

export default withRouter(connect(mapStateToProps)(PageTestingTest));
