import * as React from "react";
import {Link} from 'react-router-dom';

export class Page404 extends React.Component {
    render() {
        return (
            <div id="notfound">
                <div className="notfound">
                    <div className="notfound-404">
                        <h1>404</h1>
                    </div>
                    <h2>Oops! Nothing was found</h2>
                    <div className="text-center pb-5">
                        <img src="/images/logo.png" alt="Test Maker" className="img-fluid"/>
                    </div>
                    <p>The page you are looking for might have been removed had its name changed or is temporarily
                        unavailable.<br/>
                        <Link to={"/"}>Return to homepage</Link>
                    </p>
                </div>
            </div>
        );
    }
}
