import React, {Component} from 'react';
import {Content, Footer, Header} from "../../components";
import connect from "react-redux/es/connect/connect";
import {Link, withRouter} from 'react-router-dom';
import TestEditForm from "../../components/test/Form";
import {loadTests, setError, updateTest} from "../../actions";
import * as TestConstants from "../../constants/test";
import {Spinner} from "@blueprintjs/core";
import {can} from "../../helpers/user";

var _ = require('lodash');

class PageEditTest extends Component {
    componentDidMount() {
        const {dispatch, testId, test, user, history} = this.props;
        if (can(user.user)) {
            if (!test) {
                dispatch(loadTests(user.user, () => {
                }));
            }
        } else {
            dispatch(setError('Access is denied'));
            history.push("/");
        }
    }

    isEditable = () => {
        if (this.props.test) {
            switch (this.props.test.status) {
                case TestConstants.STATUS_NEW:
                case TestConstants.STATUS_READY_RUN:
                    return true;
                default:
                    return false;
            }
        }
        return false;
    };

    render() {
        return (
            <div>
                <Header/>
                <Content>
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0 text-dark">Update test # {this.props.testId}</h1>
                                </div>
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                        <li className="breadcrumb-item"><Link to="/tests">Tests</Link></li>
                                        <li className="breadcrumb-item active">Update test # {this.props.testId}</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="content">
                        <div className="container-fluid">
                            {
                                this.props.test && this.props.test.quests ?
                                    <TestEditForm test={this.props.test} submit={updateTest}
                                                  editable={this.isEditable()}/> :
                                    <Spinner intent={"primary"} size="100"/>
                            }
                        </div>
                    </div>
                </Content>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        testId: parseInt(ownProps.match.params.testId),
        test: state.test.list.items[_.findIndex(state.test.list.items, (test) => (test.id === parseInt(ownProps.match.params.testId)))],
        group: state.group
    };
};

export default withRouter(connect(mapStateToProps)(PageEditTest));
