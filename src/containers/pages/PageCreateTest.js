import React, {Component} from 'react';
import {Content, Footer, Header} from "../../components";
import connect from "react-redux/es/connect/connect";
import TestEditForm from "../../components/test/Form";
import {createTest} from "../../actions";
import {Link} from "react-router-dom";
import {createTestModel} from "../../utils";

class PageCreateTest extends Component {
    constructor() {
        super();
        this.state = {
            test: createTestModel({}),
        };
    };

    render() {
        return (
            <div>
                <Header/>
                <Content>
                    <div className="content-header">
                        <div className="container-fluid">
                            <div className="row mb-2">
                                <div className="col-sm-6">
                                    <h1 className="m-0 text-dark">Create new test</h1>
                                </div>
                                <div className="col-sm-6">
                                    <ol className="breadcrumb float-sm-right">
                                        <li className="breadcrumb-item"><Link to="/">Home</Link></li>
                                        <li className="breadcrumb-item"><Link to="/tests">Tests</Link></li>
                                        <li className="breadcrumb-item active">Create new test</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="content">
                        <div className="container-fluid">
                            <TestEditForm test={this.state.test} submit={createTest} editable={true}/>
                        </div>
                    </div>
                </Content>
                <Footer/>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    errorMessage: state.errorMessage,
    test: state.test,
    user: state.user,
    group: state.group
});

export default connect(mapStateToProps)(PageCreateTest);
