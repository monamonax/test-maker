import React, {Component} from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import PageHome from "./pages/PageHome";
import PageUsers from "./pages/PageUsers";
import DevTools from './DevTools'
import PageTests from "./pages/PageTests";
import PageCreateTest from "./pages/PageCreateTest";
import PageViewTest from "./pages/PageViewTest";
import PageEditTest from "./pages/PageEditTest";
import {Alert} from "@blueprintjs/core";
import connect from "react-redux/es/connect/connect";
import {resetErrorMessage} from "../actions";
import {Page404} from "./pages/Page404";
import PageTestingTest from "./pages/PageTestingTest";
import {getSidebarOpenStatus} from "../utils";
import PageLogin from "./pages/PageLogin";
import PrivateRoute from "../components/PrivateRoute";
import PageResultTest from "./pages/PageResultTest";

class Root extends Component {
    static defaultProps = {
        page: {
            sidebar_open: getSidebarOpenStatus(),
        },
    };

    componentDidMount() {
        this.changeBodyClass();
    }

    componentDidUpdate(prevProps) {
        if (this.props.page != prevProps.page) {
            this.changeBodyClass();
        }
    }

    changeBodyClass = () => {
        if (this.props.page.sidebar_open) {
            document.body.classList.add('sidebar-open');
            document.body.classList.remove('sidebar-collapse');
        } else {
            document.body.classList.remove('sidebar-open');
            document.body.classList.add('sidebar-collapse');
        }
    };

    handleDismissClick = () => {
        const {dispatch} = this.props;
        dispatch(resetErrorMessage());
    };


    renderErrorMessage() {
        const {errorMessage} = this.props;
        if (!errorMessage) {
            return null;
        }

        return (
            <Alert isOpen={true} onClose={this.handleDismissClick}>{errorMessage}</Alert>
        )
    }

    render() {
        return (
            <>
                <BrowserRouter>
                    <Switch>
                        <PrivateRoute exact path="/" component={PageHome} isAuth={this.props.user.isAuth}
                                      user={this.props.user.user}/>
                        <Route exact path="/auth" component={PageLogin}/>
                        <PrivateRoute path="/users" component={PageUsers} isAuth={this.props.user.isAuth}
                                      user={this.props.user.user}/>
                        <PrivateRoute path="/tests/create" component={PageCreateTest} isAuth={this.props.user.isAuth}
                                      user={this.props.user.user}/>
                        <PrivateRoute path="/tests/:testId/results" component={PageResultTest}
                                      isAuth={this.props.user.isAuth} user={this.props.user.user}/>
                        <PrivateRoute path="/tests/:testId/testing" component={PageTestingTest}
                                      isAuth={this.props.user.isAuth} user={this.props.user.user}/>
                        <PrivateRoute path="/tests/:testId/update" component={PageEditTest}
                                      isAuth={this.props.user.isAuth} user={this.props.user.user}/>
                        <PrivateRoute path="/tests/:testId" component={PageViewTest} isAuth={this.props.user.isAuth}
                                      user={this.props.user.user}/>
                        <PrivateRoute path="/tests" component={PageTests} isAuth={this.props.user.isAuth}
                                      user={this.props.user.user}/>
                        <Route component={Page404}/>
                    </Switch>
                </BrowserRouter>
                {this.renderErrorMessage()}
                <DevTools/>
            </>
        );
    }
}

const mapStateToProps = (state, ownProps) => ({
    errorMessage: state.errorMessage,
    test: state.test,
    user: state.user,
    group: state.group,
    page: state.page,
});

export default connect(mapStateToProps)(Root);

