import React from 'react'
import {render} from 'react-dom'
import 'normalize.css/normalize.css';
import 'admin-lte/plugins/font-awesome/css/font-awesome.min.css';
import 'admin-lte/dist/css/adminlte.min.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import Root from './containers/Root';
import configureStore from './store/configureStore';
import Provider from "react-redux/es/components/Provider";
import './index.css';

Array.prototype.clone = function () {
    return this.slice(0);
};
const store = configureStore();

render(
    <Provider store={store}>
        <Root/>
    </Provider>,
    document.getElementById('root')
);
