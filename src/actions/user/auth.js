import * as ActionTypes from "../../constants/action-types";
import * as URL from "../../constants/url";
import {createUserModel} from "../../utils";

const authBegin = (user) => {
    return {
        type: ActionTypes.USER_AUTH_BEGIN,
        payload: {user: user}
    }
};

const authEnd = (user) => {
    return {
        type: ActionTypes.USER_AUTH_END,
        payload: {user: user, isAuth: user.id != null, receivedAt: Date.now()}
    }
};

const getUserByGoogleId = (user) => dispatch => {
    dispatch(authBegin(user));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_USERS}?googleId=${user.googleId}`, {
        method: 'GET',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        }
    })
        .then(response => (response.json()))
};

const createUser = (user) => dispatch => {
    if (user.id != null) {
        return new Promise()
            .then(user => {
                dispatch(authEnd(createUserModel(user)));

                return user;
            })
    }

    return fetch(`${URL.API_URL}${URL.ENDPOINT_USERS}`, {
        method: 'POST',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            name: user.name,
            googleId: user.googleId,
            imageUrl: user.imageUrl,
            isAdmin: false,
            group: '/api/user_groups/1',
        })
    })
        .then(response => response.json())
        .then(user => {
            dispatch(authEnd(createUserModel(user)));

            return user;
        });
};

export const auth = (user, callback = () => {
}) => (dispatch) => {
    return dispatch(getUserByGoogleId(user))
        .then(userList => {
            if (userList.length > 0) {
                dispatch(authEnd(createUserModel(userList[0])));

                return userList[0];
            } else {
                return dispatch(createUser(user));
            }
        })
        .then(user => callback(user));
};
