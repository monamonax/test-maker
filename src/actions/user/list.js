import * as ActionTypes from "../../constants/action-types";
import * as URL from "../../constants/url";

const userListRequest = () => {
    return {type: ActionTypes.USER_LIST_REQUEST, payload: null}
};
const userListReceive = (json) => {
    return {
        type: ActionTypes.USER_LIST_RECEIVE,
        payload: {users: json, receivedAt: Date.now()}
    }
};
const fetchUsers = (callback = () => {
}) => dispatch => {
    dispatch(userListRequest());
    return fetch(`${URL.API_URL}${URL.ENDPOINT_USERS}`, {headers: {accept: "application/json"}})
        .then(response => response.json())
        .then(json => {
            dispatch(userListReceive(json));
            return json;
        })
        .then(json => callback(json))
};
const shouldFetchUsers = (state) => {
    const user = state.user;
    if (!user) {
        return true;
    }
    if (user.isFetching) {
        return false;
    }

    return user.didInvalidate;
};

export const loadUsers = (callback) => (dispatch, getState) => {
    if (shouldFetchUsers(getState())) {
        return dispatch(fetchUsers(callback))
    }
};
