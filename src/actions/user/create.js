import * as ActionTypes from "../../constants/action-types";
import * as URL from "../../constants/url";

const userCreateReceive = (user) => {
    return {
        type: ActionTypes.USER_CREATE_RECEIVE,
        payload: {user: user, receivedAt: Date.now()}
    }
};

const fetchUserCreate = (user, callback = () => {
}) => dispatch => {
    return fetch(`${URL.API_URL}${URL.ENDPOINT_USERS}`, {
        method: 'post',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            name: user.name,
            googleId: user.googleId,
            imageUrl: user.imageUrl,
            isAdmin: false,
        })
    })
        .then(response => response.json())
        .then(json => {
            dispatch(userCreateReceive(json));
            return json;
        })
        .then((json) => callback(json))
};

export const createUser = (user, callback) => (dispatch) => {
    return dispatch(fetchUserCreate(user, callback))
};
