import * as ActionTypes from "../../constants/action-types";

export const logout = () => {
    return {
        type: ActionTypes.USER_LOGOUT,
        payload: {}
    }
};
