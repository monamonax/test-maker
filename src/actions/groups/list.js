import * as ActionTypes from "../../constants/action-types";
import * as URL from "../../constants/url";

const groupsListRequest = () => {
    return {type: ActionTypes.GROUP_LIST_REQUEST, payload: null}
};
const groupsListReceive = (json) => {
    return {
        type: ActionTypes.GROUP_LIST_RECEIVE,
        payload: {users: json, receivedAt: Date.now()}
    }
};
const fetchUserGroups = (callback = () => {
}) => dispatch => {
    dispatch(groupsListRequest());
    return fetch(`${URL.API_URL}${URL.ENDPOINT_GROUPS}`, {headers: {accept: "application/json"}})
        .then(response => response.json())
        .then(json => {
            dispatch(groupsListReceive(json));
            return json;
        })
        .then(json => callback(json))
};
const shouldFetchUserGroups = (state) => {
    const group = state.group;
    if (!group) {
        return true;
    }
    if (group.isFetching) {
        return false;
    }

    return group.didInvalidate;
};

export const loadUserGroups = (callback) => (dispatch, getState) => {
    if (shouldFetchUserGroups(getState())) {
        return dispatch(fetchUserGroups(callback))
    }
};
