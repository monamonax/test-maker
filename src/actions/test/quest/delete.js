import * as ActionTypes from "../../../constants/action-types";
import * as URL from "../../../constants/url";

const questDeleteRequest = (quest, testId) => {
    return {type: ActionTypes.QUEST_DELETE_REQUEST, payload: {quest: quest, testId: testId}}
};
const questDeleteReceive = (status, quest, testId) => {
    return {
        type: ActionTypes.QUEST_DELETE_RECEIVE,
        payload: {status: status, quest: quest, testId: testId, receivedAt: Date.now()}
    }
};
const fetchQuestDelete = (quest, testId, callback = () => {
}) => dispatch => {
    dispatch(questDeleteRequest(quest, testId));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_QUEST}/${quest.id}`, {
        method: 'delete',
        headers: {accept: "application/json"}
    })
        .then(response => response.status)
        .then(status => {
            dispatch(questDeleteReceive(status, quest, testId));
            return status;
        })
        .then(status => callback(quest, testId))
};

export const deleteQuest = (quest, testId, callback) => (dispatch) => {
    return dispatch(fetchQuestDelete(quest, testId, callback))
};
