import * as ActionTypes from "../../../constants/action-types";
import * as URL from "../../../constants/url";

const questCreateRequest = (quest) => {
    return {type: ActionTypes.QUEST_CREATE_REQUEST, payload: {quest: quest}}
};
const questCreateReceive = (quest, testId) => {
    return {
        type: ActionTypes.QUEST_CREATE_RECEIVE,
        payload: {quest: quest, testId: testId, receivedAt: Date.now()}
    }
};
const fetchQuestCreate = (quest, callback = () => {
}) => dispatch => {
    dispatch(questCreateRequest(quest));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_QUEST}`, {
        method: 'post',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            type: quest.type,
            sort: quest.sort,
            data: quest.data,
            test: `/api/tests/${quest.testId}`,
            price: quest.price
        })
    })
        .then(response => response.json())
        .then(json => {
            dispatch(questCreateReceive(json, quest.testId));
            return json;
        })
        .then((json) => callback(json))
};

export const createQuest = (quest, callback) => (dispatch) => {
    return dispatch(fetchQuestCreate(quest, callback))
};
