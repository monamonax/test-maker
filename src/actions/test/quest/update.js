import * as ActionTypes from "../../../constants/action-types";
import * as URL from "../../../constants/url";

const questUpdateRequest = (quest) => {
    return {type: ActionTypes.QUEST_UPDATE_REQUEST, payload: {quest: quest}}
};
const questUpdateReceive = (quest, testId) => {
    return {
        type: ActionTypes.QUEST_UPDATE_RECEIVE,
        payload: {quest: quest, testId: testId, receivedAt: Date.now()}
    }
};
const fetchQuestUpdate = (quest, testId, callback = () => {
}) => dispatch => {
    dispatch(questUpdateRequest(quest, testId));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_QUEST}/${quest.id}`, {
        method: 'put',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            data: quest.data,
            sort: quest.sort,
            price: quest.price
        })
    })
        .then(response => response.json())
        .then(json => {
            dispatch(questUpdateReceive(json, testId));
            return json;
        })
        .then((json) => callback(json))
};

export const updateQuest = (quest, testId, callback) => (dispatch) => {
    return dispatch(fetchQuestUpdate(quest, testId, callback))
};
