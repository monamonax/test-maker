import * as ActionTypes from "../../../constants/action-types";
import * as URL from "../../../constants/url";

const questListRequest = (testId) => {
    return {type: ActionTypes.QUEST_LIST_REQUEST, payload: {testId: testId}}
};
const questListReceive = (testId, json) => {
    return {
        type: ActionTypes.QUEST_LIST_RECEIVE,
        payload: {testId: testId, quests: json, receivedAt: Date.now()}
    }
};

const fetchQuestList = (testId, callback = () => {
}) => dispatch => {
    dispatch(questListRequest(testId));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_QUEST}?test.id=${testId}`, {headers: {accept: "application/json"}})
        .then(response => response.json())
        .then(json => {
            dispatch(questListReceive(testId, json));
            return json;
        })
        .then(json => callback(json))
};

export const loadQuestsByTestId = (testId, callback) => (dispatch) => {
    return dispatch(fetchQuestList(testId, callback))
};
