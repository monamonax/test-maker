import * as ActionTypes from "../../constants/action-types";
import * as URL from "../../constants/url";

const testUpdateRequest = (test) => {
    return {type: ActionTypes.TEST_UPDATE_REQUEST, payload: {test: test}}
};
const testUpdateReceive = (test) => {
    return {
        type: ActionTypes.TEST_UPDATE_RECEIVE,
        payload: {test: test, receivedAt: Date.now()}
    }
};

const fetchTestUpdate = (test, callback = () => {
}) => dispatch => {
    dispatch(testUpdateRequest(test));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_TEST}/${test.id}`, {
        method: 'put',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            title: test.title,
            description: test.description,
        })
    })
        .then(response => response.json())
        .then(json => {
            dispatch(testUpdateReceive(json));
            return json;
        })
        .then((json) => callback(json))
};

export const updateTest = (test, callback) => (dispatch) => {
    return dispatch(fetchTestUpdate(test, callback))
};
