import * as ActionTypes from "../../constants/action-types";
import * as URL from "../../constants/url";
import {createTestModel} from "../../utils";

const testListRequest = () => {
    return {type: ActionTypes.TEST_LIST_REQUEST, payload: null}
};
const testListReceive = (json) => {
    return {
        type: ActionTypes.TEST_LIST_RECEIVE,
        payload: {tests: json, receivedAt: Date.now()}
    }
};

const fetchTestList = (user, callback = () => {
}) => dispatch => {
    dispatch(testListRequest());
    let filter = '';
    if (!user.isAdmin) {
        filter += `groups.id=${user.group}&status[]=5&status[]=6`;
    }
    return fetch(`${URL.API_URL}${URL.ENDPOINT_TEST}?${filter}`, {headers: {accept: "application/json"}})
        .then(response => response.json())
        .then(json => {
            let tests = [];
            json.forEach((test) => {
                tests.push(createTestModel(test));
            });
            dispatch(testListReceive(tests));

            return tests;
        })
        .then((tests) => callback(tests))
};

const shouldFetchTests = (state) => {
    const test = state.test;
    if (!test) {
        return true;
    }
    if (test.list.isFetching) {
        return false;
    }

    return test.list.didUpdate;
};

export const loadTests = (user, callback = () => {
}) => (dispatch, getState) => {
    if (shouldFetchTests(getState())) {
        return dispatch(fetchTestList(user, callback))
    }
};
