import * as ActionTypes from "../../constants/action-types";
import * as URL from "../../constants/url";
import {loadQuestsByTestId} from "./quest";

const testChangeStatusRequest = (testId, statusId) => {
    return {type: ActionTypes.TEST_CHANGE_STATUS_REQUEST, payload: {testId: testId, statusId: statusId}}
};
const testChangeStatusReceive = (testId, statusId, test) => {
    return {
        type: ActionTypes.TEST_CHANGE_STATUS_RECEIVE,
        payload: {testId, statusId, test: test, receivedAt: Date.now()}
    }
};

const fetchTestChangeStatus = (testId, statusId, callback = () => {
}) => dispatch => {
    dispatch(testChangeStatusRequest(testId, statusId));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_TEST}/${testId}`, {
        method: 'put',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            status: statusId,
        })
    })
        .then(response => response.json())
        .then(json => {
            dispatch(testChangeStatusReceive(testId, statusId, json));
            return json;
        })
        .then(json => {
            dispatch(loadQuestsByTestId(testId));
            return json;
        })
        .then((json) => callback(json))
};

export const changeStatusTest = (testId, statusId, callback) => (dispatch) => {
    return dispatch(fetchTestChangeStatus(testId, statusId, callback))
};
