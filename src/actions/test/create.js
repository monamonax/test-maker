import * as ActionTypes from "../../constants/action-types";
import * as URL from "../../constants/url";

const testCreateRequest = (test) => {
    return {type: ActionTypes.TEST_CREATE_REQUEST, payload: {test: test}}
};
const testCreateReceive = (test) => {
    return {
        type: ActionTypes.TEST_CREATE_RECEIVE,
        payload: {test: test, receivedAt: Date.now()}
    }
};

const fetchTestCreate = (test, callback = () => {
}) => dispatch => {
    dispatch(testCreateRequest(test));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_TEST}`, {
        method: 'post',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            title: test.title,
            description: test.description,
            time: 300,
            groups: [
                '/api/user_groups/1',
            ],
        })
    })
        .then(response => response.json())
        .then(json => {
            dispatch(testCreateReceive(json));
            return json;
        })
        .then((json) => callback(json))
};

export const createTest = (test, callback) => (dispatch) => {
    return dispatch(fetchTestCreate(test, callback))
};
