import * as ActionTypes from "../../constants/action-types";
import * as URL from "../../constants/url";
import {STATUS_DELETE} from "../../constants/test";

const testDeleteRequest = (test) => {
    return {type: ActionTypes.TEST_DELETE_REQUEST, payload: {test: test}}
};
const testDeleteReceive = (status, test) => {
    return {
        type: ActionTypes.TEST_DELETE_RECEIVE,
        payload: {status: status, test: {...test, status: STATUS_DELETE}, receivedAt: Date.now()}
    }
};
const fetchTestDelete = (test, callback = () => {
}) => dispatch => {
    dispatch(testDeleteRequest(test));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_TEST}/${test.id}`, {
        method: 'delete',
        headers: {accept: "application/json"}
    })
        .then(response => response.status)
        .then(status => {
            dispatch(testDeleteReceive(status, test));
            return status;
        })
        .then(status => callback(status, test))
};

export const deleteTest = (test, callback) => (dispatch) => {
    return dispatch(fetchTestDelete(test, callback))
};
