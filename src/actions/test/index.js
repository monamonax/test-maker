import {loadTests} from './list';
import {createTest} from './create';
import {deleteTest} from './delete';
import {updateTest} from './update';
import {changeStatusTest} from './changeStatus';

export {
    loadTests,
    createTest,
    deleteTest,
    updateTest,
    changeStatusTest
};
