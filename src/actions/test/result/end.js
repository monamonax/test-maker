import * as ActionTypes from "../../../constants/action-types";
import * as URL from "../../../constants/url";
import {createResultModel} from "../../../utils";

const resultResultRequest = (result) => {
    return {type: ActionTypes.RESULT_END_REQUEST, payload: {result: result}}
};
const resultResultReceive = (result, testId, userId) => {
    return {
        type: ActionTypes.RESULT_END_RECEIVE,
        payload: {result: result, testId: testId, userId: userId, receivedAt: Date.now()}
    }
};
const resultResultEnd = (result, callback = () => {
}) => dispatch => {
    dispatch(resultResultRequest(result));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_RESULT}/${result.id}`, {
        method: 'put',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "end": true,
        })
    })
        .then(response => response.json())
        .then(json => {
            const result = createResultModel(json);
            dispatch(resultResultReceive(result, result.testId, result.userId));

            return result;
        })
        .then((result) => callback(result))
};

export const endResult = (result, callback) => (dispatch) => {
    return dispatch(resultResultEnd(result, callback))
};
