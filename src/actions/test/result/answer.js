import * as ActionTypes from "../../../constants/action-types";
import * as URL from "../../../constants/url";
import {createResultModel} from "../../../utils";

const resultAnswerRequest = (answer) => {
    return {type: ActionTypes.RESULT_ANSWER_REQUEST, payload: {answer: answer}}
};
const resultAnswerReceive = (result, testId, userId) => {
    return {
        type: ActionTypes.RESULT_ANSWER_RECEIVE,
        payload: {result: result, testId: testId, userId: userId, receivedAt: Date.now()}
    }
};
const resultResultAnswer = (answer, callback = () => {
}) => dispatch => {
    dispatch(resultAnswerRequest(answer));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_RESULT}/${answer.resultId}/answer/${answer.questId}`, {
        method: 'post',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "answer": answer.answer,
        })
    })
        .then(response => response.json())
        .then(json => {
            const result = createResultModel(json);
            dispatch(resultAnswerReceive(result, result.testId, result.userId));

            return result;
        })
        .then((result) => callback(result))
};

export const answerResult = (answer, callback) => (dispatch) => {
    return dispatch(resultResultAnswer(answer, callback))
};
