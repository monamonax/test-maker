import * as ActionTypes from "../../../constants/action-types";
import * as URL from "../../../constants/url";
import {createResultModel} from "../../../utils";

const resultResultRequest = (result) => {
    return {type: ActionTypes.RESULT_CREATE_REQUEST, payload: {result: result}}
};
const resultResultReceive = (result, testId, userId) => {
    return {
        type: ActionTypes.RESULT_CREATE_RECEIVE,
        payload: {result: result, testId: testId, userId: userId, receivedAt: Date.now()}
    }
};
const resultResultCreate = (result, callback = () => {
}) => dispatch => {
    dispatch(resultResultRequest(result));
    return fetch(`${URL.API_URL}${URL.ENDPOINT_RESULT}`, {
        method: 'post',
        headers: {
            accept: "application/json",
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            "test": `/api/tests/${result.testId}`,
            "user": `/api/users/${result.userId}`,
            "data": [],
            "result": []
        })
    })
        .then(response => response.json())
        .then(json => {
            const result = createResultModel(json);
            dispatch(resultResultReceive(result, result.testId, result.userId));

            return result;
        })
        .then((result) => callback(result))
};

export const createResult = (result, callback) => (dispatch) => {
    return dispatch(resultResultCreate(result, callback))
};
