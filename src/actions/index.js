import * as ActionTypes from "../constants/action-types";

import {createQuest, deleteQuest, loadQuestsByTestId, updateQuest} from './test/quest';
import {answerResult, createResult, endResult} from './test/result';

import {changeStatusTest, createTest, deleteTest, loadTests, updateTest} from './test';

import {loadUsers} from './user';

import {loadUserGroups} from './groups';

import {collapseSidebar} from './page';

export {
    loadQuestsByTestId,
    createQuest,
    updateQuest,
    deleteQuest,
    loadTests,
    createTest,
    updateTest,
    changeStatusTest,
    deleteTest,
    loadUsers,
    loadUserGroups,
    collapseSidebar,
    createResult,
    answerResult,
    endResult
};

export const resetErrorMessage = () => ({type: ActionTypes.RESET_ERROR_MESSAGE});
export const setError = (error) => ({type: ActionTypes.SET_ERROR, error: error});

