import * as ActionTypes from "../../constants/action-types";

const collapseSidebarAction = (oldStatus) => {
    return {
        type: ActionTypes.PAGE_SIDEBAR_COLLAPSE,
        payload: {sidebar_open: !oldStatus}
    }
};

export const collapseSidebar = () => (dispatch, getState) => {
    return dispatch(collapseSidebarAction(getState().page.sidebar_open))
};
