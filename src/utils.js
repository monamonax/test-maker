import * as React from "react";

export const createTestModel = (test) => {
    let groups = test.groups;
    if (test.groupsAsArray) {
        groups = [];
        test.groupsAsArray.forEach((group) => {
            groups.push(createGroupModel(group));
        });
        delete test.groupsAsArray;
    }
    let quests = test.quests;
    if (test.questsAsArray) {
        quests = [];
        test.questsAsArray.forEach((quest) => {
            quests.push(createQuestModel(quest));
        });
        delete test.questsAsArray;
    }
    let results = test.results;
    if (test.resultsAsArray) {
        results = [];
        test.resultsAsArray.forEach((result) => {
            results.push(createResultModel(result));
        });
        delete test.resultsAsArray;
    }

    return {
        id: null,
        title: '',
        description: '',
        status: 0,
        time: 300,
        maxPrice: 0,
        ...test,
        groups: groups,
        quests: quests,
        results: results
    };
};

export const createGroupModel = (group) => ({
    id: null,
    name: null,
    ...group,
});
export const createUserModel = (user) => {
    let group = user.group;
    if (user.groupAsArray) {
        group = createGroupModel(user.groupAsArray);
        delete user.groupAsArray;
    }
    let results = user.results;
    if (user.resultsAsArray) {
        results = [];
        user.resultsAsArray.forEach((result) => {
            results.push(createResultModel(result));
        });
        delete user.resultsAsArray;
    }

    return {
        id: null,
        name: null,
        googleId: null,
        imageUrl: null,
        isAdmin: false,
        ...user,
        group: group,
        results: results
    };
};
export const createResultModel = (result) => {
    let user = result.user;
    if (result.userAsArray) {
        user = createUserModel(result.userAsArray);
        delete result.userAsArray;
    }
    return {
        id: null,
        data: [],
        result: [],
        createAt: null,
        updateAt: null,
        is_end: false,
        balls: 0,
        ...result,
        user: user,
    };
};
export const createQuestModel = (quest) => ({
    id: null,
    type: 0,
    sort: 0,
    test: null,
    price: 1,
    data: {},
    ...quest,
    results: null
});

export const handleBooleanChange = function (handler: (checked: boolean) => void) {
    return (event: React.FormEvent) => handler((event.target).checked);
};

export const handleStringChange = function (handler: (value: string) => void) {
    return (event: React.FormEvent) => handler((event.target).value);
};

export const handleNumberChange = function (handler: (value: number) => void) {
    return handleStringChange(value => handler(+value));
};

export const getSidebarOpenStatus = function () {
    return getLocalStorageByKey('sidebar_open', false) == 'true';
};

export const setSidebarOpenStatus = function (status) {
    setLocalStorageByKey('sidebar_open', status)
};

export const getLocalStorageByKey = function (key, defaultValue) {
    const value = localStorage.getItem(key);
    if (value != null) {
        return value;
    }

    return defaultValue;
};

export const setLocalStorageByKey = function (key, value) {
    localStorage.setItem(key, value);
};

export const removeLocalStorageByKey = function (key) {
    localStorage.removeItem(key);
};
